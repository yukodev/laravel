<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')
                ->references('id')
                ->on('categories')->onDelete('cascade');
            $table->integer('order')->nullable();
            $table->string('name');
            $table->string('name_trans')->nullable();
            $table->string('url')->nullable();
            $table->enum('target', ['_blank', '_self'])->default('_self');
            $table->text('short_text')->nullable();
            $table->text('text')->nullable();
            $table->string('breadcrumbs', 255)->nullable();
            $table->string('img')->nullable();
            $table->string('alt')->nullable();
            $table->tinyInteger('watch')->default('1');
            $table->string('seo_title')->nullable();
            $table->text('seo_description')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->longText('src')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
