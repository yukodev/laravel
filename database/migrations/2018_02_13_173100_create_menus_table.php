<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order')->nullable();
            $table->string('name');
            $table->string('name_trans')->nullable();
            $table->tinyInteger('root')->default('1');
            $table->string('controller')->nullable();
            $table->string('method')->nullable();
            $table->string('param')->nullable();
            $table->string('url')->nullable();
            $table->enum('target', ['_blank', '_self'])->default('_self');
            $table->text('short_text')->nullable();
            $table->text('text')->nullable();
            $table->tinyInteger('watch')->default('1');
            $table->string('seo_title')->nullable();
            $table->text('seo_description')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
