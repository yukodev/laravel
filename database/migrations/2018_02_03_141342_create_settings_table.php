<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->tinyInteger('forbiden')->default('0');
            $table->tinyInteger('single')->default('0');
            $table->tinyInteger('use_value')->default('1');
            $table->tinyInteger('use_short_text')->default('0');
            $table->tinyInteger('use_text')->default('0');
            $table->tinyInteger('use_editor')->default('0');
            $table->tinyInteger('use_img')->default('0');
            $table->tinyInteger('use_link')->default('0');
            $table->tinyInteger('use_link_name')->default('0');
            $table->tinyInteger('use_date')->default('0');
            $table->tinyInteger('use_email')->default('0');
            $table->tinyInteger('use_phone')->default('0');
            $table->tinyInteger('use_class')->default('0');
            $table->timestamps();
        });

        Schema::create('settings_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("setting_id")->unsigned()->index();
            $table->foreign('setting_id')
                ->references('id')
                ->on('settings')
                ->onDelete('cascade');
            $table->tinyInteger('watch')->default('1');
            $table->integer("order")->nullable();
            $table->string('value')->nullable();
            $table->string('link')->nullable();
            $table->string('link_name')->nullable();
            $table->string('img')->nullable();
            $table->string('alt')->nullable();
            $table->text('short_text')->nullable();
            $table->text('text')->nullable();
            $table->enum('link_target', ['_blank', '_self'])->default('_self');
            $table->timestamp('date')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->longText('src')->nullable();
            $table->string('class')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
