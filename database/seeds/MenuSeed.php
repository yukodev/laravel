<?php

use Illuminate\Database\Seeder;
use App\Menu;

class MenuSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $object = Menu::create([
            'name' => 'Main',
            'name_trans' => 'Main',
            'controller' => 'WelcomeController',
            'method' => 'index',
            'root' => '1',
            'text' => 'Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum ',
        ]);
        $object = Menu::create([
            'name' => 'Contacts',
            'name_trans' => 'Contacts',
            'controller' => 'contacts',
            'method' => 'index',
            'root' => '1',
            'text' => 'Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum ',
        ]);
    }
}
