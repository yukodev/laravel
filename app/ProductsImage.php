<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsImage extends Model
{
    protected $fillable = ['product_id', 'url', 'name', 'order'];

    //
    /*
     * get table column
     */
    public function get($column)
    {
        return $this->$column;
    }


    /*
     * get related object
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
