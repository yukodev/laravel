<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingsValue extends Model
{
    protected $fillable = ['value','class', 'order', 'link', 'link_name', 'img','src', 'alt',
        'short_text', 'text', 'link_target', 'date','email','phone','setting_id'];

    //
    /*
     * get table column
     */
    public function get($column)
    {
        return $this->$column;
    }

    /*
     * get all adm values
     */
    public function adm_values()
    {
        return $this->get();
    }

    /*
     * get related object
     */
    public function setting()
    {
        return $this->belongsTo('App\Setting');
    }
}
