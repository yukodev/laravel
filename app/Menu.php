<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Collective\Html\FormFacade;
use App\MenusMenu;

class Menu extends Model
{
    protected $fillable = ['name', 'name_trans', 'watch', 'short_text', 'text', 'url',
        'target', 'seo_title', 'seo_description', 'seo_keywords', 'root'];

    /**
     * Get the related menus through menus_menus table
     */
    public function menus_menus()
    {
        // return $this->hasMany('App\MenusMenu')->;
        return $this->hasManyThrough(
            'App\Menu',//menu model
            'App\MenusMenu',//menus_menus(through) table
            'menu_id',//first key of menus_menus
            'id',//3d table (menu) key
            'id',
            'next_menu_id');// far key of menus_menus
    }

    /* recursive function
     * @return generates nestable html
     */
    public function generate_nestable($str = "", $root = 1)
    {
        if ($this->id) {//generate item html
            $id = $this->id;
            $name = $this->name;
            if ($this->watch == 0) {
                $iw = "<a title='" . __("global.turn_on") . "' data-orm='Menu' data-id='{$this->id}'  class=\"btn btn-xs btn-warning toggleWatch\">" . __("global.not_act") . "</a>\n";
            } else {
                $iw = "<a title='" . __("global.turn_off") . "' data-orm='Menu' data-id='{$this->id}' class=\"btn btn-xs btn-success toggleWatch\">" . __("global.act") . "</a>\n";
            }

            $f = FormFacade::open(array(
                'style' => 'display: inline-block;',
                'method' => 'DELETE',
                'onsubmit' => "return confirm('" . trans("global.app_are_you_sure") . "');",
                'route' => ['admin.menus.destroy', $this->get('id')]));
            $f .= FormFacade::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger'));
            $f .= FormFacade::close();
            $str .= "<li class=\"category_item dd-item dd3-item\" data-id=\"$this->id\">\n";
            $str .= "                                            <div class=\"dd-handle dd3-handle\"></div>\n";
            $str .= "                                            <div class=\"dd3-content\">\n";
            $str .= "{$this->get('name')}\n";
            $str .= "                                                <div class=\"tools_col\">\n";
            $str .= "  $f\n";
            $str .= "                                                </div>\n";
            $str .= "                                                <div class=\"tools_col\">\n";
            $str .= "<a href=" . route('admin.menus.edit', [$this->get('id')]) . " class=\"btn btn-xs btn-info\">" . __('global.app_edit') . "</a>\n";
            $str .= "                                                </div>\n";
            $str .= "                                                <div class=\"tools_col\">\n";
            $str .= "                                                  $iw\n";
            $str .= "                                                </div>\n";
            $str .= "                                            </div>\n";
            $next = $this->adm_childs();
            if (count($next) > 0) {
                $str .= "<ol class=\"dd-list\">\n";
            }
            foreach ($next as $one) {
                $str = $one->generate_nestable($str, $root);
            }
            if (count($next) > 0) {
                $str .= "</ol>\n";
            }
            $str .= "</li>\n";

        } else {//load root items and re-run objects
            $next = $this->adm_roots($root);
            foreach ($next as $one) {
                $str = $one->generate_nestable($str, $root);
            }
        }
        return $str;
    }

    /*
     * loads root menus
     * @param int $root default=1
     */
    public function adm_roots($root = 1)
    {
        return
            $this->where("root", $root)->get();

    }

    /*
    * loads root published menus
    * @param int $root default=1
    */
    public function roots($root = 1)
    {
        return
            $this->where(
                [
                    "root" => $root,
                    "watch" => "1"
                ]
            )->get();

    }

    /*
     * load all related childs
     */
    public function adm_childs()
    {
        return $this->menus_menus;
    }

    /*
     * get published childs
     */
    public function childs()
    {
        return $this
            ->menus_menus
            ->where("watch", "1");
    }

    /*
     * get table column
     */
    public function get($column)
    {
        return $this->$column;
    }

    public static function refreshMenusTree($request)
    {
        MenusMenu::query()->truncate();//clear all relation
        self::refreshRecursive($request);//recursive create new relations on menus_menus table
    }

//recursive create new relations on menus_menus table
    static function refreshRecursive($request, $i = 0, $pid = false)
    {
        foreach ($request as $one) {
            $i++;
            $id = $one->id;
            $em = Menu::where("id", $id)->first();
            if (!$em->id) {// no object - send relode command (maybe someone delete it recently)
                die(json_encode(array("reload" => true, "id" => $id)));
            }
            if ($pid) {/// if got father - clear the root
                $em->root = 0;
            } else {//if becomes root clear father rels
                $em->root = 1;
                MenusMenu::where('next_menu_id', $id)->delete();//clear relation
            }
            $em->order = $i;
            $em->save();
            if ($pid) { //check we have relation to given father, add if it's have not
                $rel = MenusMenu::where("next_menu_id", $em->id)->first();
                if ($rel === null) {
                    $rel = new MenusMenu;
                    $rel->menu_id = $pid;
                    $rel->next_menu_id = $em->id;
                    $rel->save();
                }
            }
            if (isset($one->children)) {
                self::refreshRecursive($one->children, $i, $em->id);
            } else {/// no childs then delete exists
                MenusMenu::where('menu_id', $id)->delete();//clear relation
            }
        }
    }

    /**
     * generate html recursiv menus tree using menus dependencies
     * @param int $root
     * @param string $html
     * @return HTML
     */
    public function generate_menus($root = 1, $html = "")
    {

        if ($this->id) {//generate item html
            $id = $this->id;
            $name = $this->get('name');
            $url = self::get_url($this);//get url depending on menu type
            $html .= "<li>\n";
            $html .= "<a href=\"$url\">$name</a>\n";
            $next = $this->childs();
            if (count($next) > 0) {
                $html .= " <ul>\n";
            }
            foreach ($next as $one) {
                $html = $one->generate_menus($root, $html);
            }
            if (count($next) > 0) {
                $html .= "</ul>\n";
            }
            $html .= "</li>\n";

        } else {//load root items and re-run objects
            $next1 = $this->roots($root);

            foreach ($next1 as $one) {
                $html = $one->generate_menus($root, $html);
            }

        }

        return $html;
        /*  $tree = "";
          $tree .= "<ul>\n";
          $tree .= "                <li><a href=\"#\">Кнопка меню</a></li>\n";
          $tree .= "                <li class=\"active\">\n";
          $tree .= "                    <a href=\"#\">Кнопка меню</a>\n";
          $tree .= "                    <ul>\n";
          $tree .= "                        <li><a href=\"#\">Кнопка подменю 1</a></li>\n";
          $tree .= "                        <li><a href=\"#\">Кнопка подменю 2</a></li>\n";
          $tree .= "                        <li><a href=\"#\">Кнопка подменю 3</a></li>\n";
          $tree .= "                        <li><a href=\"#\">Кнопка подменю 4</a></li>\n";
          $tree .= "                        <li><a href=\"#\">Кнопка подменю 5</a></li>\n";
          $tree .= "                    </ul>\n";
          $tree .= "                </li>\n";
          $tree .= "                <li><a href=\"#\">Кнопка меню</a></li>\n";
          $tree .= "                <li><a href=\"#\">Кнопка меню</a></li>\n";
          $tree .= "                <li><a href=\"#\">Кнопка меню</a></li>\n";
          $tree .= "                <li><a href=\"#\">Кнопка меню</a></li>\n";
          $tree .= "            </ul>\n";*/

    }

    /* generate menu url depending on menu type
     * @param object $menu
     * @return string URL
     */
    private static function get_url(Menu $menu)
    {
        if ($menu->controller) {
            return route($menu->controller);
        } else {
            return route('menus', ['name_trans' => $menu->name_trans, 'id' => $menu->id]);
        }
    }

    /*
     * get system object by system variables
     * @param string Controller
     * @param string Method
     * @return object - menu table
     */
    public static function ControllerMethod($controller, $method = "")
    {
        return self::where([
            'controller' => $controller,
            'method' => $method,
            'watch' => '1'
        ])->first();
    }

    /* get object by given fields or throw 404
     * @param array associative array of fields
     * @return object|404
     */
    public static function by_fields(array $array)
    {
        return self::where($array)->firstOrFail();
    }
}
