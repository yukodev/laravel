<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Setting
 * @package App\Models\Db
 */
class Setting extends Model
{
    /**
     * Get the related settings_values
     */
    public function settings_values()
    {
        return $this->hasMany('App\SettingsValue');
    }

    //get settings values where forbiden field = 0
    public function scopeAdm_active()
    {
        return $this::where("forbiden", '0')->get();
    }

    //get one active setting
    public function one($id)
    {
        return $this
            ->where(
                [
                    "forbiden" => '0',
                    "id" => $id
                ]
            )
            ->first();
    }

    /*
     * get table column
     */
    public function get($column)
    {
        if ($column === 'name' && $this->description)//in case setting has description - return it
            return $this->description;
        else
            return $this->$column;
    }

    //get all settings values
    public function adm_values()
    {
        return $this->settings_values;
    }

    /*
     * Get first SettingsValue from given Setting by name
     * @param string name
     * return SettingsValue object
     */
    public static function FirstValue($name){
        return static::where('name',$name)
            ->first()
            ->settings_values
            ->where('watch',"1")
            ->first();
    }

    /*
     * Get all SettingsValue from given Setting by name
     * @param string name
     * return SettingsValue object
     */
    public static function AllValues($name){
        return static::where('name',$name)
            ->first()
            ->settings_values
            ->where('watch',"1")
            ->all();
    }
}
