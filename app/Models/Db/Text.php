<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Text extends Model
{
    protected $fillable = ['name', 'name_trans', 'texts_type_id', 'watch', 'short_text', 'text', 'url',
        'target', 'seo_title', 'seo_description', 'seo_keywords', 'root', 'src'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('id', 'desc');
        });
    }
    //
    //count all for pagination
    public static function objects($limit = 2)
    {
        return self::paginate($limit);
    }


    /*
     * get table column
     */
    public function get($column)
    {
        return $this->$column;
    }
}
