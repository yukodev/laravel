<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Product extends Model
{
    protected $fillable = ['name', 'category_id', 'name_trans', 'texts_type_id', 'watch', 'short_text', 'text', 'url',
        'target', 'seo_title', 'seo_description', 'seo_keywords', 'root', 'src'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('id', 'desc');
        });
    }

    //count all for pagination
    public static function objects($limit = 2)
    {
        return self::paginate($limit);
    }
    /**
     * Get the related products_images
     */
    public function products_images()
    {
        return $this->hasMany('App\ProductsImage');
    }

    /**
     * @param array $ids
     */
    public function createPictures($ids=[])
    {

        foreach($ids as $id) {

           $picture= Picture::find($id);
           if($picture!==null){
               $data=[
                   'product_id'=>$this->id,
                   'url'=>$picture->url,
                   'name'=>$picture->name
               ];
               try {

                   ProductsImage::create($data);
                   $picture->delete();
               } catch (\Exception $e) {

               }
           }
        }
    }


    /*
     * get table column
     */
    public function get($column)
    {
        return $this->$column;
    }
}
