<?php

namespace App\Repositories\Text;

use App\Models\Db\Text;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;

/**
 * Class TextRepository
 * @package App\Repositories\Text
 */
class TextRepository
{
    /**
     * @param Text $text
     * @param array $data
     * @return Text|false
     */
    public function update(Text $text, array $data)
    {
        \DB::beginTransaction();

        try {
            if ($text->update($data)) {

               /* if ($setting->maintenance && !app()->isDownForMaintenance()) {
                    Artisan::call('down', ['--message' => __('front.maintenance.general')]);
                } elseif (!$setting->maintenance && app()->isDownForMaintenance()) {
                    Artisan::call('up');
                }*/

                \DB::commit();

                return $text;
            }

        } catch (\Exception $e) {
            report($e);
        }

        \DB::rollBack();

        return false;
    }
}