<?php

namespace App\Repositories\Setting;

use App\Models\Db\Setting;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;

/**
 * Class SettingRepository
 * @package App\Repositories\Setting
 */
class SettingRepository
{
    /**
     * @param Setting $setting
     * @param array $data
     * @return Setting|false
     */
    public function update(Setting $setting, array $data)
    {
        \DB::beginTransaction();

        try {
            if ($setting->update($data)) {

               /* if ($setting->maintenance && !app()->isDownForMaintenance()) {
                    Artisan::call('down', ['--message' => __('front.maintenance.general')]);
                } elseif (!$setting->maintenance && app()->isDownForMaintenance()) {
                    Artisan::call('up');
                }*/

                \DB::commit();

                return $setting;
            }

        } catch (\Exception $e) {
            report($e);
        }

        \DB::rollBack();

        return false;
    }
}