<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TextsType extends Model
{
    //
    /**
     * @param $column
     * @return mixed
     */
    public function get($column)
    {
        return $this->$column;
    }

    /**
     * get associative array of two fields key=>value
     *
     * @param $value
     * @param $key
     * @return mixed
     */
    public static function keyValue($value, $key)
    {
        return self//::where("watch", "1")
            ::pluck($value, $key)
            ->toArray();
    }
}
