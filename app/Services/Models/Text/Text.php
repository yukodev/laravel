<?php

namespace App\Services\Models\Text;

use App\Repositories\Text\TextRepository;
use App\Services\Models\Text\Contracts\TextInterface;

/**
 * Class Text
 * @package App\Services\Models\Text
 */
class Text implements TextInterface
{
    /** @var TextRepository  */
    protected $textRepository;

    public function __construct(TextRepository $textRepository)
    {
        /** @var  textRepository */
        $this->textRepository = $textRepository;
    }

    public function index()
    {

    }

    public function store()
    {

    }

    public function create()
    {

    }

    public function show()
    {

    }

    public function update()
    {

    }

    public function destroy()
    {

    }

    public function edit()
    {

    }
}