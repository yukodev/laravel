<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 27.11.2017
 * Time: 14:15
 */

namespace App\Services\Models\Text\Contracts;

/**
 * Interface TextInterface
 * @package App\Services\Models\Text\Contracts
 */
interface TextInterface
{

    public function index();

    public function store();

    public function create();

    public function show();

    public function update();

    public function destroy();

    public function edit();

}