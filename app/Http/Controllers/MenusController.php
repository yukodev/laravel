<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class MenusController extends FatherController
{
    //
    /*
     * generate one menu object page
     * @param Request $request
     * @return html menu object page
     */
    public function index(Request $request)
    {
        $id = $request->route('id');
        $name_trans = $request->route('name_trans');
        $object = Menu::by_fields(['name_trans' => $name_trans, 'id' => $id, 'watch' => 1]);
        return view('frontend.menus', compact(['object']));
    }
}
