<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

#use App\Mail\SendMailMailable;
#use App\Jobs\SendEmailJob;


class AjaxController extends Controller
{
    //
    /* SEND message from contacts form
     * @param Illuminate\Http\Request
     * @return json
     */
    public function mailer(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email'
        ]);
        $time = Carbon::now()->addMinute(1);
        Mail::to('yuko.dev@gmail.com')->later($time, new ContactMail($request));
        $a = json_encode([
            'alert' => __('global.front.message_sent')
        ]);
        die($a);
        return true;
    }
}
