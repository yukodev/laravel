<?php

namespace App\Http\Controllers;
use App\Setting;
use App\Menu;
use Illuminate\Http\Request;

class ContactsController extends FatherController
{
    //

    public function index(){
        $object = Menu::ControllerMethod('ContactsController', 'index');
        $social = Setting::AllValues('social');
        $contacts_map = Setting::FirstValue('contacts_map');
        $use_footer=false;
        return view('frontend.contacts',
            compact(
                ['object','social','use_footer','contacts_map']
            )
        );
    }
}
