<?php

namespace App\Http\Controllers;

use App\Setting;
use App\Menu;
use Carbon\Carbon;
use Illuminate\Http\Request;

//use Illuminate\Support\Facades\Mail;
//use App\Jobs\SendEmailJob;


class WelcomeController extends FatherController
{
    //
    /*
     * display welcome page
     */
    public function index()
    {

        $object = Menu::ControllerMethod('WelcomeController', 'index');
        $welcome_slogan = Setting::FirstValue('welcome_slogan');
        $promo_title = Setting::FirstValue('promo_title');
        $advantages_left = Setting::AllValues('advantages_left');
        $advantages_right = Setting::AllValues('advantages_right');
        $promo_items = Setting::AllValues('promo_items');
        return view('frontend.welcome',
            compact(
                ['welcome_slogan', 'advantages_left', 'advantages_right', 'object', 'promo_title', 'promo_items']
            )
        );
    }
}
