<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Traits\AdminTrait;

class SettingsControlle extends Controller
{
    use AdminTrait; // load data used on each page

    public function __construct()
    {
        $this->parse_data();// load data used on each page using AdminTrait
    }
    //
    /*
     * display list of settings values using settings $setting_id
     */
    public function list($setting_id)
    {
        $object = new Setting();
        $set = $object->one($setting_id);
        if (!$set)
            abort(404);
        $setts = $set->all();
        return view('admin.settings.index', compact(['set', 'setts']));
    }
}
