<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Menu;

class FatherController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $menus = new Menu();
        View()->share([
            'site_name' => Setting::FirstValue('site_name'),
            'phones' => Setting::AllValues('phones'),
            'header_data' => Setting::FirstValue('header_data'),
            'menus' => $menus->generate_menus(1),
            'use_footer' => true
        ]);
    }
}
