<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UpdateMenusRequest;
use App\Http\Requests\Admin\StoreMenusRequest;
use App\Menu;
use App\Http\Traits\AdminTrait;

class MenusController extends Controller
{
    use AdminTrait; // load data used on each page

    public function __construct()
    {
        $this->parse_data();// load data used on each page using AdminTrait
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $object = new Menu();
        $nestable = $object->generate_nestable();
        return view('admin.menus.tree', compact(['nestable']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $object = new Menu();
        $route = ['admin.menus.store'];
        $title = __('global.menus.create');
        $method = 'POST';
        return view('admin.menus.create', compact(['object', 'route', 'method', 'title']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\StoreMenusRequest
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMenusRequest $request)
    {

        $object = Menu::create($request->all());
        return redirect()->route('admin.menus.index');
        //return redirect()->route('admin.menus.show', [$object->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $object = Menu::findOrFail($id);
        $route = ['admin.menus.update', $id];
        $title = __('global.menus.edit') . " " . $object->get('name');
        $method = 'PUT';
        return view('admin.menus.create', compact(['object', 'route', 'method', 'title']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\UpdateMenusRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMenusRequest $request, $id)
    {

        $object = Menu::findOrFail($id);
        $object->update($request->all());
        return redirect()->route('admin.menus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = Menu::findOrFail($id);
        $object->delete();
        return redirect()->route('admin.menus.index');
    }


}
