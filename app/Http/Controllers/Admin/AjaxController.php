<?php

namespace App\Http\Controllers\admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;

class AjaxController extends Controller
{
    //
    /**
     * Update relations between parent-child menus tree.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function refreshMenusTree(Request $request)
    {
        $strRequest = file_get_contents('php://input');
        $req = json_decode($strRequest);
        Menu::refreshMenusTree($req);
        // print_r($req);
        die(json_encode(['alert' => __('global.alert.changesSaved')]));
    }


    /**
     * Update relations between parent-child menus tree.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function refreshCategoriesTree(Request $request)
    {
        $strRequest = file_get_contents('php://input');
        $req = json_decode($strRequest);
        Category::refreshMenusTree($req);
        // print_r($req);
        die(json_encode(['alert' => __('global.alert.changesSaved')]));
    }

    /**
     * Toggle watch field using given orm and id
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function toggleWatch(Request $request)
    {
        $class = 'App\\' . $_POST['orm'];
        $object = $class::find($_POST['id']);
        if (!$object)
            die(json_encode(['reload' => true]));
        if ($object->watch == 1) {
            $object->watch = 0;
            $className = 'btn-warning';
            $text = __("global.not_act");
        } else {
            $object->watch = 1;
            $className = 'btn-success';
            $text = __("global.act");
        }
        $object->save();
        die(json_encode([
            'alert' => __('global.alert.changesSaved'),
            'className' => $className,
            'text' => $text
        ]));
    }
}
