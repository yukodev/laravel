<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;

/**
 * Class Base
 * @package App\Http\Controllers\Admin
 */
class Base extends Controller
{
    /**
     * Base constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        view()->share(
            [
                'settings' => Setting::adm_active()
            ]
        );
    }
}
