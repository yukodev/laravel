<?php

namespace App\Http\Controllers\admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UpdateMenusRequest;
use App\Http\Requests\Admin\StoreMenusRequest;
use App\Menu;
use App\Http\Traits\AdminTrait;

class CatalogController extends Controller
{
    use AdminTrait; // load data used on each page

    public function __construct()
    {
        $this->parse_data();// load data used on each page using AdminTrait
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $object = new Category();
        $nestable = $object->generate_nestable();
        return view('admin.catalog.tree', compact(['nestable']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $object = new Category();
        $route = ['admin.catalog.store'];
        $title = __('global.catalog.create');
        $method = 'POST';
        return view('admin.catalog.create', compact(['object', 'route', 'method', 'title']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\StoreMenusRequest
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMenusRequest $request)
    {

        Category::create($request->all());
        return redirect()->route('admin.catalog.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $object = Category::findOrFail($id);
        $route = ['admin.catalog.update', $id];
        $title = __('global.menus.edit') . " " . $object->get('name');
        $method = 'PUT';
        return view('admin.catalog.create', compact(['object', 'route', 'method', 'title']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\UpdateMenusRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMenusRequest $request, $id)
    {

        $object = Category::findOrFail($id);
        $object->update($request->all());
        return redirect()->route('admin.catalog.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = Category::findOrFail($id);
        $object->delete();
        return redirect()->route('admin.catalog.index');
    }


}
