<?php

namespace App\Http\Controllers\Admin;

use App\Setting;
use App\SettingsValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UpdateSettingsValueRequest;
use App\Http\Requests\Admin\StoreSettingsValueRequest;

use App\Http\Traits\AdminTrait;

class SettingsController extends Controller
{
    use AdminTrait; // load data used on each page

    public function __construct()
    {
        $this->parse_data();// load data used on each page using AdminTrait
    }
    //
    /*
     * display list of settings values using settings $setting_id
     */
    public function show($setting_id)
    {
        // $c=SettingsValue::all();
        // dd($c);
        $object = new Setting();
        $set = $object->one($setting_id);
        if (!$set)
            abort(404);
        $setts = $set->adm_values();//get all settings values
        return view('admin.settings.index', compact(['set', 'setts']));
    }

    /**
     * Remove SettingsValue from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $object = SettingsValue::findOrFail($id);
        $sid = $object->setting_id;
        $object->delete();
        return redirect()->route('admin.settings.show', [$sid]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $object = new Setting();
        $father = Setting::findOrFail($_GET['setting_id']);
        $route = ['admin.settings.store'];
        $title = __('global.settings.create');
        $method = 'POST';
        return view('admin.settings.edit', compact(['object', 'route', 'method', 'title','father']));
    }

    /**
     * Show the form for editing SettingsValue.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //if (!Gate::allows('users_manage')) {
        //     return abort(401);
        // }
        //  $roles = Role::get()->pluck('name', 'name');

        $object = SettingsValue::findOrFail($id);
        $father = $object->setting;
        $route = ['admin.settings.update', $id];
        $title = __('global.settings.edit') . " " . $object->get('name');
        $method = 'PUT';
        return view('admin.settings.edit', compact('object', 'father','route', 'method', 'title'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSettingsValueRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSettingsValueRequest $request)
    {

        $object = SettingsValue::create($request->all());
        $sid = $object->setting_id;
        return redirect()->route('admin.settings.show', [$sid]);
        //return redirect()->route('admin.menus.show', [$object->id]);
    }

    /**
     * Update SettingValue in storage.
     *
     * @param  \App\Http\Requests\UpdateSettingsValueRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSettingsValueRequest $request, $id)
    {
        //if (!Gate::allows('users_manage')) {
        //    return abort(401);
        //}
        // dd($request);
        $object = SettingsValue::findOrFail($id);
        $object->update($request->all());
        $sid = $object->setting_id;
        return redirect()->route('admin.settings.show', [$sid]);
    }
}
