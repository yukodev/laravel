<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UpdateTextsRequest;
use App\Http\Requests\Admin\StoreTextsRequest;
use App\Http\Traits\AdminTrait;
use App\Text;
use App\TextsType;

class TextsController extends Base
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //
        $objects = Text::objects();
        return view('admin.texts.index', compact(['objects']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $object = new Text();
        $texts_types = TextsType::keyValue('name', 'id');
        $route = ['admin.texts.store'];
        $title = __('global.texts.create');
        $method = 'POST';
        return view('admin.texts.edit', compact(['object', 'texts_types', 'route', 'method', 'title']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTextsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTextsRequest $request)
    {
        //
        $object = Text::create($request->all());
        return redirect()->route('admin.texts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $object = Text::findOrFail($id);
        $texts_types = TextsType::keyValue('name', 'id');
        $route = ['admin.texts.update', $id];
        $title = __('global.texts.edit') . " " . $object->get('name');
        $method = 'PUT';

        return view('admin.texts.edit', compact(['object', 'texts_types', 'route', 'method', 'title']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTextsRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTextsRequest $request, $id)
    {
        //

        $object = Text::findOrFail($id);
        $object->update($request->all());
        return redirect()->back()->with('msgs', [__('global.app_changes_saved')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = Text::findOrFail($id);
        $object->delete();
        return redirect()->back();
    }
}
