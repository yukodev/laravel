<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\admin\StoreProductsRequest;
use App\Http\Requests\admin\UpdateProductsRequest;
use App\Http\Traits\AdminTrait;
use App\Product;
use App\ProductsImage;
use Croppa;

class ProductsController extends Controller
{
    use AdminTrait; // load data used on each page
    public $folder = '/uploads/'; // add slashes for better url handling

    public function __construct()
    {
        $this->parse_data();// load data used on each page using AdminTrait
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $objects = Product::objects();
        return view('admin.products.index', compact(['objects']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $object = new Product();
        $categories = Category::keyValue('name', 'id');
        $route = ['admin.products.store'];
        $title = __('global.texts.create');
        $method = 'POST';
        return view('admin.products.edit', compact(['object', 'categories', 'route', 'method', 'title']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTextsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductsRequest $request)
    {
        //
        $object = Product::create($request->all());
        $object->createPictures($request->get('fileid'));
        return redirect()->route('admin.products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $object = Product::findOrFail($id);
        $categories = Category::keyValue('name', 'id');
        $images = $object->products_images->map(function ($image) {
            $image['deleteUrl'] = route('admin.productPicture.destroy',[$image['id']]);
            return $image;
        });
        $route = ['admin.products.update', $id];
        $title = __('global.products.edit') . " " . $object->get('name');
        $method = 'PUT';

        return view('admin.products.edit', compact(['object', 'categories', 'route', 'method', 'title', 'images']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTextsRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductsRequest $request, $id)
    {
        //
        $object = Product::findOrFail($id);
        $object->update($request->all());
        $object->createPictures($request->get('fileid'));
        return redirect()->back()->with('msgs', [__('global.app_changes_saved')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = Product::findOrFail($id);
        $object->delete();
        return redirect()->back();
    }


    public function destroyPictu1re($id)
    {
        $object = ProductPictue::findOrFail($id);
        $object->delete();
        return redirect()->back();
    }

    public function destroyPicture(ProductsImage $productsImage)
    {
        Croppa::delete($productsImage->url); // delete file and thumbnail(s)
        $productsImage->delete(); // delete db record
        return response()->json([$productsImage->url]);
    }
}
