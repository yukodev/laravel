<?php

namespace App\Http\Traits;

use App\Setting;
use Illuminate\View\View;

trait AdminTrait
{
    public $settings;//=Setting::all();

    public function parse_data()
    {
        View()->share(
            [
                'settings' => Setting::adm_active()
            ]
        );
        // Get all the brands from the Brands Table.
        // $brands = Brand::all();

        // return $brands;
    }
}