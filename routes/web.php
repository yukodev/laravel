<?php

use App\Events\eventTrigger;
Route::get('/','WelcomeController@index')->name('WelcomeController');
Route::get('/contacts','ContactsController@index')->name('ContactsController');
Route::get('/menus/{name_trans}-{id}','MenusController@index')->name('menus');
Route::post('ajax/mailer','AjaxController@mailer')->name('mailer');

// Admin base page
Route::get('/admin', function () {
    return redirect('/admin/home');
});

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::resource('pictures', 'Admin\PictureController');
    Route::put('pictures', 'Admin\PictureController@store')->name('pictures.store');
    Route::delete('products_images/{productsImage}', 'Admin\ProductsController@destroyPicture')->name('productPicture.destroy');

    Route::get('/home', 'HomeController@index');
    Route::resource('settings', 'Admin\SettingsController');
    Route::resource('menus', 'Admin\MenusController');
    Route::resource('catalog', 'Admin\CatalogController');
    Route::resource('products', 'Admin\ProductsController');
    Route::resource('texts', 'Admin\TextsController');
    Route::post('ajax/refreshCategoriesTree','Admin\AjaxController@refreshCategoriesTree')->name('refreshCategoriesTree');
    Route::post('ajax/refreshMenusTree','Admin\AjaxController@refreshMenusTree')->name('refreshMenusTree');
    Route::post('ajax/toggleWatch','Admin\AjaxController@toggleWatch')->name('toggleWatch');

    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);

});

Route::get('/alertBox', function () {
    return view('eventListener');
});
Route::get('/fireEvent', function () {
event(new eventTrigger());
});