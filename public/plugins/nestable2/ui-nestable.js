var UINestable = function () {
    var send;
    var jsonStructre;
    //function to initiate jquery.nestable
    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');

        if (window.JSON) {

            var send = window.JSON.stringify(list.nestable('serialize'));
            if (send != defnest)// make changes only if tree changed
            {
                if ($('#tree_type').val() == "menu") {
                    var method = "refresh_tree_menu/" + $("#root").val();
                } else {
                    var method = "refresh_tree";
                }
                console.log(window.JSON.stringify(list.nestable('serialize')));
                $.ajax({
                    type: "POST",
                    url: "/" + $("#lang").val() + "/admin/xhr_office/" + method,
                    data: send,
                    dataType: "json",
                    success: function (answer) {
                        if (answer.reload) {
                            window.location.reload();
                        }
                        if (answer.message) {
                            msg(answer.message);
                        }
                        defnest = send;//refresh def structure variable
                    }
                });
            }
            // output.val(window.JSON.stringify(list.nestable('serialize')));
            //, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };
    var runNestable = function () {
        // activate Nestable for list 1
        $('#nestable').nestable({
            group: 1,
            maxDepth: 20
        }).on('change', updateOutput);
        //store defoult structure for compare after moving the tree
        var e = $('#nestable').data('output', $('#nestable-output'));
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        defnest = window.JSON.stringify(list.nestable('serialize'));
        //!

        // output initial serialised data
        //    updateOutput($('#nestable').data('output', $('#nestable-output')));
        $('#nestable-menu').on('click', function (e) {
            var target = $(e.target),
                action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });
    };
    var runNestable2 = function () {//initialize nestable engine
        $('#nestable').nestable({
            beforeDragStop: function (l, e, p) {
                // console.log(e);
                //console.log(p);
                // l is the main container
                // e is the element that was moved
                // p is the place where element was moved.
            },
            callback: function (l, e) {
                send = window.JSON.stringify(l.nestable('serialize'));
                if (jsonStructre === send) {//structure is same - do no thing
                    console.log('the same!');
                }
                else {//run Ajax to store changes to db
                    console.log('its differs');
                    updateDB();
                }
                console.log(send);

            }
        });
        //store defoult structure for compare after moving the tree
        jsonStructre = window.JSON.stringify($('#nestable').nestable('serialize'));
        //  $('#nestable').nestable('expandAll');
    };
    var updateDB = function () {
        $.ajax({
            type: "POST",
            url: nestableUrl,
            dataType: 'json',
            data: send,
            success: function (data) {
                console.log(data);
            },
            onError: function (e) {

            }
        });
        jsonStructre = send;
        //    alert(send);
    };
    return {
        //main function to initiate template pages
        init: function () {
            runNestable2();
        }
    };
}();