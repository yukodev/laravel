<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description : Controller of Welcome page
 * Respons URL : http://domain/welcome
 * 
 */
class Controller_Admin_Xhr_Office extends Controller_Admin_Mainajax {

    public function before() {
        parent::before();
        if (!Request::$is_ajax) {
            die(__('not_allowed'));
        }
        if (!Request::$is_ajax) {
            die(json_encode(array("reload" => true)));
        }
        $this->auto_render = false;
        I18n::lang('admin_' . Kohana::config('admin/locale.lang'));
    }

    public function action_toggle_product_activity() {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("product")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->watch == 0) {
                    $arr->watch = 1;
                    $title = __("turn_off");
                } else {
                    $arr->watch = 0;
                    $title = __("turn_on");
                }
                $arr->save();
                die(json_encode(array("watch" => $arr->watch, "title" => $title)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_toggle_sw_activity() {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("statics_data")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->watch == 0) {
                    $arr->watch = 1;
                    $title = __("turn_off");
                } else {
                    $arr->watch = 0;
                    $title = __("turn_on");
                }
                $arr->save();
                die(json_encode(array("watch" => $arr->watch, "title" => $title)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_toggle_productc_activity() {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("products_comment")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->watch == 0) {
                    $arr->watch = 1;
                    $title = __("turn_off");
                } else {
                    $arr->watch = 0;
                    $title = __("turn_on");
                }
                $arr->save();
                die(json_encode(array("watch" => $arr->watch, "title" => $title)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_toggle_cat_activity() {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("category")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->watch == 0) {
                    $arr->watch = 1;
                    $title = __("turn_off");
                } else {
                    $arr->watch = 0;
                    $title = __("turn_on");
                }
                $arr->save();
                die(json_encode(array("watch" => $arr->watch, "title" => $title)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_toggle_delivery_activity($lang) {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("payment_method")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->watch == 0) {
                    $arr->watch = 1;
                    $title = __("turn_off");
                } else {
                    $arr->watch = 0;
                    $title = __("turn_on");
                }
                $arr->save();
                die(json_encode(array("watch" => $arr->watch, "title" => $title)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_multi_product_activity_on() {
        $pids = arr::get($_POST, "pids");
        $json = array();
        foreach ($pids as $one) {
            $arr = ORM::factory("product")->where("id", "=", $one)->find();
            if ($arr->id) {
                $arr->watch = 1;
                $title = __("turn_off");
                $arr->save();
                $json[] = array("watch" => $arr->watch, "title" => $title, "id" => $arr->id);
            }
        }
        die(json_encode($json));
    }

    public function action_multi_product_activity_off() {
        $pids = arr::get($_POST, "pids");
        $json = array();
        foreach ($pids as $one) {
            $arr = ORM::factory("product")->where("id", "=", $one)->find();
            if ($arr->id) {
                $arr->watch = 0;
                $title = __("turn_on");
                $arr->save();
                $json[] = array("watch" => $arr->watch, "title" => $title, "id" => $arr->id);
            }
        }
        die(json_encode($json));
    }

    public function action_delete_product() {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("product")->where("id", "=", $id)->find();
            if ($arr->id) {
                if (self::remove_product($arr))
                    die(json_encode(array("okay" => true)));
                else
                    die(json_encode(array("reload" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_delete_productc() {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("products_comment")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->delete())
                    die(json_encode(array("okay" => true)));
                else
                    die(json_encode(array("reload" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_delete_order() {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("order")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->delete())
                    die(json_encode(array("okay" => true)));
                else
                    die(json_encode(array("reload" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_delete_delivery($lang) {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("payment_method")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->delete())
                    die(json_encode(array("okay" => true)));
                else
                    die(json_encode(array("reload" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_delete_cat() {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("category")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->delete())
                    die(json_encode(array("okay" => true)));
                else
                    die(json_encode(array("reload" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_multi_delete_product() {
        $pids = arr::get($_POST, "pids");
        foreach ($pids as $one) {
            $arr = ORM::factory("product")->where("id", "=", $one)->find();
            if ($arr->id) {
                self::remove_product($arr);
            }
        }
        die(json_encode(array("okay" => true)));
    }

    public function remove_product($arr) {
        if ($arr->delete()) {
            return true;
        } else {
            return false;
        }
    }

    public static function watermark($path = 'uploads/watermark.png', $img_width = 100, $img_height = 100, $rate = 0.7, $x = 0.1, $y = 0) {
        $mark = Image::factory($path);
        $mark_w = $img_width * $rate;
        $mark_h = $img_height * $rate;
        return $mark->resize($mark_w, $mark_h);
    }

    public function action_save_product($lang) {
        $id = (int) $_POST["pid"];
        $name = $lang . "_name";
        $short_text = $lang . "_short_text";
        $text = $lang . "_text";
        $seo_title = $lang . "_seo_title";
        $seo_description = $lang . "_seo_description";
        $seo_keywords = $lang . "_seo_keywords";
        $arr = ORM::factory("product")->where("id", "=", $id)->find();

        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('name', 'name');
        //    ->label('name_trans', 'name_trans');

        $valid->rule('name', 'not_empty')
                ->rule('name', 'min_length', array(2))
                ->rule('name', 'max_length', array(100));
        /*  $valid->rule('name_trans', 'not_empty')
          ->rule('name_trans', 'min_length', array(2))
          ->rule('name_trans', 'max_length', array(100))
          ->rule('name_trans', 'Model_Product::check_name_trans', array($arr->id))
          ->rule('name_trans', 'alpha_dash'); */


        $valid_form = $valid->check();
        if ($valid_form) {

            //-----------TAB1
            $arr->name = trim($_POST["name"]);
            $arr->$name = trim($_POST["$name"]);
            $arr->articul = trim($_POST["articul"]);
            $arr->$short_text = trim($_POST["$short_text"]);
            $arr->$text = trim($_POST["$text"]);
            $arr->order = (int) trim($_POST["order"]);
            $arr->watch = $_POST["watch"];
            //-----------TAB2
            $arr->price_base = (double) $_POST["price_base"];
            $arr->currency_id = $_POST["currency_id"];
            $arr->price = (double) $_POST["price"];
            $arr->price_old = (double) $_POST["price_old"];
            $arr->quantity = $_POST["quantity"];
            $arr->category_id = $_POST["category_id"];
            if (trim($_POST['hmain_img'])) {
                $ava = trim($_POST['hmain_img']);
                $file = 'uploads/users/' . $ava;
                if (file_exists($file)) {
                    $ava = trim($_POST['hmain_img']);
                    if ($arr->img) {
                        @unlink("uploads/products/img/" . $arr->img);
                        @unlink("uploads/products/thumb/" . $arr->img);
                        @unlink("uploads/products/small/" . $arr->img);
                    }
                    $image = Image::factory($file);
                    //WM
                    $watermark = orm::factory("static")->get("watermark")->get_one_value();
                    if ($watermark->id) {
                        $BASIC_IMG_H = $image->height;
                        $BASIC_IMG_W = $image->width;
                        $watermark = self::watermark('uploads/settings/' . $watermark->img, $BASIC_IMG_W, $BASIC_IMG_H, 1);
                        $pos_x = ($BASIC_IMG_H / 2) - ($watermark->height / 2);
                        $pos_y = ($BASIC_IMG_W / 2) - ($watermark->width / 2);
                        $result = $image->watermark($watermark, $pos_y, $pos_x);
                    }

                    $newfile = 'uploads/products/img/' . $ava;
                    $result = $image->save($newfile);
                    $newfile = 'uploads/products/thumb/' . $ava;
                    $image->resize(280, null);
                    $result = $image->save($newfile);
                    $newfile = 'uploads/products/small/' . $ava;
                    $image->resize(100, null);
                    $result = $image->save($newfile);
                    //   copy($file, $newfile);
                    @unlink($file);
                    $arr->img = $ava;
                }
            }
            //-----------TAB3
            $arr->$seo_title = $_POST["$seo_title"];
            $arr->$seo_description = $_POST["$seo_description"];
            $arr->$seo_keywords = $_POST["$seo_keywords"];
            $arr->bc = implode("-", $_POST["bc"]);
            $arr->save();
            $arr->name_trans = helper_utils::str2url(trim($_POST["name"])) . "-" . $arr->id;
            $arr->save();
            //trim($_POST["name_trans"]);

            if ($arr->id) {
                $time = time();
                foreach ($_POST["photos"] as $key => $one) {
                    $file = explode("files", $one);
                    $file = "files/" . $file[1];
                    if (file_exists($file)) {
                        $image = Image::factory($file);
                        $watermark = orm::factory("static")->get("watermark")->get_one_value();
                        if ($watermark->id) {
                            $BASIC_IMG_H = $image->height;
                            $BASIC_IMG_W = $image->width;
                            $watermark = self::watermark('uploads/settings/' . $watermark->img, $BASIC_IMG_W, $BASIC_IMG_H, 1);
                            $pos_x = ($BASIC_IMG_H / 2) - ($watermark->height / 2);
                            $pos_y = ($BASIC_IMG_W / 2) - ($watermark->width / 2);
                            $result = $image->watermark($watermark, $pos_y, $pos_x);
                        }
                        if ($image->width > 1000) {
                            $image->resize(1000, null);
                        }
                        if ($image->height > 1000) {
                            $image->resize(null, 1000);
                        }
                        $na = explode("/", $one);
                        $name = $na[count($na) - 1];

                        $na = explode(".", $name);
                        $name1 = $na[count($na) - 2];
                        $image->save("uploads/products/img/$time" . $name);


                        if ($image->width > 88) {
                            $image->resize(88, null);
                        }
                        if ($image->height > 80) {
                            $image->resize(null, 80);
                        }
                        $image->save("uploads/products/thumb/$time" . $name);
                        $ph = orm::factory("products_photo");
                        $ph->product_id = $arr->id;
                        $ph->img = $time . $name;
                        $ph->watch = 1;
                        $ph->save();

                        @unlink($file);
                        @unlink($file2);
                    }
                }
            }
            $em = orm::factory("category")
                    ->where("id", "=", $_POST["category_id"])
                    ->find();
            //-----------paams
            //create new
            $ex = $arr->products_parameters->find_all();
            //delete parameters been turned of
            foreach ($ex as $one1) {
                if (!in_array($one1->parameters_value_id, $_POST["p_" . $one1->parameter_id])) {
                    $query = DB::delete('products_parameters')
                            ->where('product_id', "=", $arr->id)
                            ->and_where('parameters_value_id', "=", $one1->parameters_value_id)
                            ->execute();
                }
            }

            foreach (orm::factory("parameter")->find_all() as $one) {
                foreach ($_POST["p_" . $one->id] as $val) {
                    /* if ($one->is_slider == 1) {
                      $pobject = ORM::factory("products_parameter");
                      $pobject->product_id = $arr->id;
                      $pobject->parameter_id = $one->id;
                      $pobject->parameters_value_id = null;
                      $pobject->value = $val;
                      $pobject->save();
                      } elseif ($one->is_text == 1) {
                      if ($val != "") {
                      $pobject = ORM::factory("products_parameter");
                      $pobject->product_id = $arr->id;
                      $pobject->parameter_id = $one->id;
                      $pobject->parameters_value_id = null;
                      $pobject->text = $val;
                      $pobject->save();
                      }
                      } else { */
                    $pobject = ORM::factory("products_parameter")
                            ->where("product_id", "=", $arr->id)
                            ->and_where("parameter_id", "=", $one->id)
                            ->and_where("parameters_value_id", "=", $val)
                            ->find();
                    if (!$pobject->id) {
                        $pobject->product_id = $arr->id;
                        $pobject->parameter_id = $one->id;
                        $pobject->parameters_value_id = $val;
                        $pobject->save();
                    }
                    //}
                }
            }
            $ex = $arr->products_attributes->find_all();
            //delete attributes been turned of
            foreach ($ex as $one1) {
                if (!in_array($one1->attributes_value_id, $_POST["attr" . $one1->attribute_id])) {
                    $query = DB::delete('products_attributes')
                            ->where('product_id', "=", $arr->id)
                            ->and_where('attributes_value_id', "=", $one1->attributes_value_id)
                            ->execute();
                }
            }
            foreach (orm::factory("attribute")->find_all() as $one) {
                foreach ($_POST["attr" . $one->id] as $val) {
                    /* if ($one->is_slider == 1) {
                      $pobject = ORM::factory("products_parameter");
                      $pobject->product_id = $arr->id;
                      $pobject->parameter_id = $one->id;
                      $pobject->parameters_value_id = null;
                      $pobject->value = $val;
                      $pobject->save();
                      } elseif ($one->is_text == 1) {
                      if ($val != "") {
                      $pobject = ORM::factory("products_parameter");
                      $pobject->product_id = $arr->id;
                      $pobject->parameter_id = $one->id;
                      $pobject->parameters_value_id = null;
                      $pobject->text = $val;
                      $pobject->save();
                      }
                      } else { */
                    $pobject = ORM::factory("products_attribute")
                            ->where("product_id", "=", $arr->id)
                            ->and_where("attribute_id", "=", $one->id)
                            ->and_where("attributes_value_id", "=", $val)
                            ->find();
                    $pobject->product_id = $arr->id;
                    $pobject->attribute_id = $one->id;
                    $pobject->attributes_value_id = $val;
                    $pobject->value = $_POST["attr_price"][$one->id . "-" . $val];
                    $pobject->save();
                    //}
                }
            }

            //----------!params
            //COLORS-------------------------------------------------------
            /* $ex = array();
              foreach ($arr->colors->find_all() as $col) {
              $ex[] = $col->id;
              }
              $nex = array();
              foreach ($_POST["colors"] as $col) {
              $nex[] = $col;
              }
              $new = array_diff($nex, $ex);
              $del = array_diff($ex, $nex);
              if (count($del) > 0)
              $query = DB::delete('products_colors')
              ->where('color_id', DB::expr("IN"), $del)
              ->execute();
              if (count($new) > 0)
              foreach ($new as $one) {
              $rel = orm::factory("products_color");
              $rel->color_id = $one;
              $rel->product_id = $arr->id;
              $rel->save();
              } */
            //parameters-------------------------------------------------------
            /*     $ex = array();
              foreach ($this->user->parameters->find_all() as $one) {
              $ex[] = $one->id;
              }
              $nex = array();
              foreach ($_POST["parameters"] as $one) {
              $nex[] = $one;
              }
              $new = array_diff($nex, $ex);
              $del = array_diff($ex, $nex);
              if (count($del) > 0) {
              $query = DB::delete('users_parameters')
              ->where('parameter_id', DB::expr("IN"), $del)
              ->execute();

              $query = DB::delete('products_parameters')
              ->where('parameter_id', DB::expr("IN"), $del)
              ->and_where('product_id', '=', $arr->id)
              ->execute();
              }
              if (count($new) > 0)
              foreach ($new as $one) {
              $rel = orm::factory("users_parameter");
              $rel->user_id = $this->user->id;
              $rel->parameter_id = $one;
              $rel->save();
              } */
            /* $arr->remove_all("colors");
              foreach ($_POST["colors"] as $item) {
              $ar1 = ORM::factory("color")->where("id", "=", $item)->find();
              $ar1->add("products", $arr);
              } */
            //------------!colors
            if ($em->id) {
                $query = DB::delete('categories_products')
                        ->where('product_id', "=", $arr->id)
                        ->execute();
                $rel = orm::factory("categories_product")
                        //     ->where("eshop_menu_id", "=", $em->id)
                        ->and_where("product_id", "=", $arr->id)
                        ->find();
                $rel->category_id = $em->id;
                $rel->product_id = $arr->id;
                $rel->save();
                $engine = new Helper_Catalog_Scanner($arr); //generate breadcrumbs 
            }
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
        //   }
        //  else
        //    die(json_encode(array("reload" => true)));

        die(json_encode(array("id" => $arr->id)));
    }

    public function action_save_product_comment($lang) {

        $id = (int) $_POST["pid"];
        $name = $lang . "_name";
        $text = $lang . "_text";
        $arr = ORM::factory("products_comment")->where("id", "=", $id)->find();

        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('name', 'name');
        //    ->label('name_trans', 'name_trans');

        $valid->rule('name', 'not_empty')
                ->rule('name', 'min_length', array(2))
                ->rule('name', 'max_length', array(100));
        /*  $valid->rule('name_trans', 'not_empty')
          ->rule('name_trans', 'min_length', array(2))
          ->rule('name_trans', 'max_length', array(100))
          ->rule('name_trans', 'Model_Product::check_name_trans', array($arr->id))
          ->rule('name_trans', 'alpha_dash'); */


        $valid_form = $valid->check();
        if ($valid_form || 1) {

            //-----------TAB1
            $arr->name = trim($_POST["name"]);
            $arr->title = trim($_POST["title"]);
            $arr->$name = trim($_POST["$name"]);
            $arr->$text = trim($_POST["$text"]);
            $arr->email = trim($_POST["email"]);
            $arr->phone = trim($_POST["phone"]);
            $arr->comment = trim($_POST["comment"]);
            $arr->point = trim($_POST["point"]);

            $arr->watch = $_POST["watch"];


            $arr->save();
            //trim($_POST["name_trans"]);
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
        //   }
        //  else
        //    die(json_encode(array("reload" => true)));
        $this->finish('notice', __("admin_changes_saved"));
        // die(json_encode(array("id" => $arr->id)));
    }

    public function action_save_delivery($lang) {
        $name = $lang . "_name";
        $text = $lang . "_text";
        $id = (int) $_POST["pid"];
        $arr = ORM::factory("payment_method")->where("id", "=", $id)->find();
        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('name', 'name')
                ->label('name_trans', 'name_trans');
        $valid->rule('name', 'not_empty')
                ->rule('name', 'min_length', array(2))
                ->rule('name', 'max_length', array(100));
        $valid_form = $valid->check();
        if ($valid_form) {
            $arr->user_id = $this->user->id;
            //-----------TAB1
            $arr->name = trim($_POST["name"]);
            $arr->$name = trim($_POST["$name"]);
            $arr->$text = trim($_POST["$text"]);
            $arr->price = trim($_POST["price"]);
            $arr->order = (int) trim($_POST["order"]);
            $arr->watch = $_POST["watch"];
            $arr->save();
            die(json_encode(array("id" => $arr->id)));
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
    }

    /*
     * saves parameter
     */

    public function action_save_paramscat($lang) {
        $name = $lang . "_name";
        $text = $lang . "_text";
        $id = (int) $_POST["pid"];
        $arr = ORM::factory("parameter")->where("id", "=", $id)->find();
        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('name', 'name')
                ->label('name_trans', 'name_trans');
        $valid->rule('name', 'not_empty')
                ->rule('name', 'min_length', array(2))
                ->rule('name', 'max_length', array(100));
        $valid_form = $valid->check();
        if ($valid_form) {
            //-----------TAB1
            $arr->name = trim($_POST["name"]);
            $arr->$name = trim($_POST["$name"]);
            $arr->$text = trim($_POST["$text"]);
            $arr->save();
            die(json_encode(array("id" => $arr->id)));
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
    }

    /*
     * saves attribute
     */

    public function action_save_attributescat($lang) {
        $name = $lang . "_name";
        $text = $lang . "_text";
        $id = (int) $_POST["pid"];
        $arr = ORM::factory("attribute")->where("id", "=", $id)->find();
        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('name', 'name')
                ->label('name_trans', 'name_trans');
        $valid->rule('name', 'not_empty')
                ->rule('name', 'min_length', array(2))
                ->rule('name', 'max_length', array(100));
        $valid_form = $valid->check();
        if ($valid_form) {
            // $arr->language_id = 1;
            //-----------TAB1
            $arr->name = trim($_POST["name"]);
            $arr->$name = trim($_POST["$name"]);
            $arr->$text = trim($_POST["$text"]);
            $arr->save();
            die(json_encode(array("id" => $arr->id)));
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
    }

    /*
     * save one parameters value
     */

    public function action_save_param($lang) {
        $value = $lang . "_value";
        $id = (int) $_POST["pid"];
        $arr = ORM::factory("parameters_value")->where("id", "=", $id)->find();
        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('value', 'value')
                ->label('parameter_id', 'parameter_id')
                ->label('name_trans', 'name_trans');
        $valid->rule('value', 'not_empty')
                //   ->rule('value', 'min_length', array(2))
                ->rule('value', 'max_length', array(100));

        $valid->rule('parameter_id', 'not_empty');

        $valid_form = $valid->check();
        if ($valid_form) {
            $arr->parameter_id = trim($_POST["parameter_id"]);
            //-----------TAB1
            $arr->value = trim($_POST["value"]);
            $arr->$value = trim($_POST["$value"]);

            $arr->save();
            die(json_encode(array("url" => "/$lang/admin/office/params/" . $_POST["parameter_id"])));
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
    }

    /*
     * saves one attributes value
     */

    public function action_save_attribute($lang) {
        $value = $lang . "_value";
        $id = (int) $_POST["pid"];
        $arr = ORM::factory("attributes_value")->where("id", "=", $id)->find();
        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('value', 'value')
                ->label('parameter_id', 'parameter_id')
                ->label('name_trans', 'name_trans');
        $valid->rule('value', 'not_empty')
                //   ->rule('value', 'min_length', array(2))
                ->rule('value', 'max_length', array(100));

        $valid->rule('parameter_id', 'not_empty');

        $valid_form = $valid->check();
        if ($valid_form) {
            $arr->attribute_id = trim($_POST["parameter_id"]);
            //-----------TAB1
            $arr->value = trim($_POST["value"]);
            $arr->$value = trim($_POST["$value"]);
            $arr->save();
            die(json_encode(array("url" => "/$lang/admin/office/attributes/" . $_POST["parameter_id"])));
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
    }

    public function action_save_options() {
        $id = (int) $_POST["pid"];

        //parameters-------------------------------------------------------
        $ex = array();
        foreach ($this->user->parameters->find_all() as $one) {
            $ex[] = $one->id;
        }
        $nex = array();
        foreach ($_POST["parameters"] as $one) {
            $nex[] = $one;
        }
        $new = array_diff($nex, $ex);
        $del = array_diff($ex, $nex);
        if (count($del) > 0) {
            $query = DB::delete('users_parameters')
                    ->where('parameter_id', DB::expr("IN"), $del)
                    ->execute();

            $query = DB::delete('products_parameters')
                    ->where('parameter_id', DB::expr("IN"), $del)
                    ->and_where('product_id', '=', $arr->id)
                    ->execute();
        }
        if (count($new) > 0)
            foreach ($new as $one) {
                $rel = orm::factory("users_parameter");
                $rel->user_id = $this->user->id;
                $rel->parameter_id = $one;
                $rel->save();
            }

        die(json_encode(array("id" => $id)));
    }

    public function action_remove_img() {
        $id = arr::get($_POST, "pid");
        if ($id) {
            $arr = ORM::factory("product")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->img) {
                    @unlink("uploads/products/img/" . $arr->img);
                    @unlink("uploads/products/thumb/" . $arr->img);
                    @unlink("uploads/products/small/" . $arr->img);
                    $arr->img = "";
                    $arr->save();
                }
                die(json_encode(array("okay" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_remove_img_category() {
        $id = arr::get($_POST, "pid");
        if ($id) {
            $arr = ORM::factory("category")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->img) {
                    @unlink("uploads/eshop/" . $arr->img);
                    $arr->img = "";
                    $arr->save();
                }
                die(json_encode(array("okay" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_remove_img_banner() {
        $id = arr::get($_POST, "pid");
        if ($id) {
            $arr = ORM::factory("banner")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->img) {
                    @unlink("uploads/settings/" . $arr->img);
                    $arr->img = "";
                    $arr->save();
                }
                die(json_encode(array("okay" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_remove_img_pub($lang) {
        $id = arr::get($_POST, "pid");
        if ($id) {
            $arr = ORM::factory("text")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->img) {
                    @unlink("uploads/docs/" . $arr->img);
                    @unlink("uploads/docs/thumb/" . $arr->img);

                    $arr->img = "";
                    $arr->save();
                }
                die(json_encode(array("okay" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_remove_img_pub_logo($lang) {
        $id = arr::get($_POST, "pid");
        if ($id) {
            $arr = ORM::factory("text")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->logo) {
                    @unlink("uploads/docs/" . $arr->logo);
                    @unlink("uploads/docs/thumb/" . $arr->logo);

                    $arr->logo = "";
                    $arr->save();
                }
                die(json_encode(array("okay" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_remove_sl() {

        if ($this->user->settings_logo) {
            @unlink("uploads/settings/" . $this->user->settings_logo);
            $this->user->settings_logo = "";
            $this->user->save();
        }
        die(json_encode(array("okay" => true)));
    }

    public function action_remove_sv() {

        if ($this->user->settings_visitka) {
            @unlink("uploads/settings/" . $this->user->settings_visitka);
            $this->user->settings_visitka = "";
            $this->user->save();
        }
        die(json_encode(array("okay" => true)));
    }

    public function action_remove_favico() {

        if ($this->user->favico) {
            @unlink("uploads/settings/" . $this->user->favico);
            $this->user->favico = "";
            $this->user->save();
        }
        die(json_encode(array("okay" => true)));
    }

    public function action_save_doska($id) {

        if ($_POST == Security::xss_clean($_POST)) {
            /* if autoreg checked - create user
             * $uid needed for assign ad to user
             */

            $obj = orm::factory("board")->save_board($_POST, $id, $uid);
            //IMAGES1

            foreach ($_POST["files"] as $one) {
                if ($one) {
                    $file = "uploads/users/" . $one;
                    if (file_exists($file)) {
                        $s = 'vdoske123456789'; // дописать строку надо
                        $random = '';
                        for ($i = 0; $i < 7; ++$i) {
                            $random .= $s{rand(0, 14)}; // 36 количество символов в масиве -1, очень важно
                        }

                        $ext = substr(strrchr($one, '.'), 1);
                        $fname = $obj->name_trans . "-" . time() . $random . "." . $ext;
                        //Do crop if it's defined
                        $image = Image::factory($file);
                        if ($image->width > 650 || $image->height > 650)
                            $image->resize(650, 650);
                        $image->save("uploads/photos/$fname");
                        $image->resize(170, 145);
                        $image->save("uploads/photos/thumb/$fname");
                        $image->resize(90, 68);
                        $image->save("uploads/photos/small/$fname");
                        //save
                        $arr = ORM::factory("boards_photo");
                        $arr->board_id = $obj->id;
                        $arr->order = ORM::factory("boards_photo")
                                        ->order_by("order", "desc")
                                        ->limit(1)->find()->order + 1;
                        $arr->watch1 = 1;
                        $arr->img = $fname;
                        $arr->alt = $obj->name;
                        $arr->save();
                    }
                }
            }
            //IMAGES2
            if ($_FILES["ads_img_1"])
                $images[] = Controller_Ajaxer_upload::action_upload_one($obj->name_trans, "ads_img_1", 170, 145);
            if ($_FILES["ads_img_2"])
                $images[] = Controller_Ajaxer_upload::action_upload_one($obj->name_trans, "ads_img_2", 170, 145);
            if ($_FILES["ads_img_3"])
                $images[] = Controller_Ajaxer_upload::action_upload_one($obj->name_trans, "ads_img_3", 170, 145);
            if ($_FILES["ads_img_4"])
                $images[] = Controller_Ajaxer_upload::action_upload_one($obj->name_trans, "ads_img_4", 170, 145);
            if ($_FILES["ads_img_5"])
                $images[] = Controller_Ajaxer_upload::action_upload_one($obj->name_trans, "ads_img_5", 170, 145);
            if ($_FILES["ads_img_6"])
                $images[] = Controller_Ajaxer_upload::action_upload_one($obj->name_trans, "ads_img_6", 170, 145);
            if ($_FILES["ads_img_7"])
                $images[] = Controller_Ajaxer_upload::action_upload_one($obj->name_trans, "ads_img_7", 170, 145);
            if ($_FILES["ads_img_8"])
                $images[] = Controller_Ajaxer_upload::action_upload_one($obj->name_trans, "ads_img_8", 170, 145);
            if ($_FILES["ads_img_9"])
                $images[] = Controller_Ajaxer_upload::action_upload_one($obj->name_trans, "ads_img_9", 170, 145);

            foreach ($images as $one) {
                if ($one) {
                    $arr = ORM::factory("boards_photo");
                    $arr->board_id = $obj->id;
                    $arr->order = ORM::factory("boards_photo")
                                    ->order_by("order", "desc")
                                    ->limit(1)->find()->order + 1;
                    $arr->watch1 = 1;
                    $arr->img = $one;
                    $arr->alt = $obj->name;
                    $arr->save();
                }
            }

            Request::instance()->redirect('/my/');
        } else
            die("XSS");
    }

    public function action_mupl() {
        $this->auto_render = false;
        error_reporting(E_ALL | E_STRICT);
        require('office-media/assets/plugins/jQuery-File-Upload/server/php/UploadHandler.php');
        $upload_handler = new UploadHandler();
    }

    public function action_delete_photo() {
        $this->auto_render = false;
        $photo = orm::factory("products_photo")
                ->where("id", "=", trim($_GET["id"]))
                ->find();
        if ($photo->id) {
            $photo->delete();
        }
        $this->finish();
    }

    public function action_delete_pages_photo() {
        $this->auto_render = false;
        $photo = orm::factory("photo")
                ->where("id", "=", trim($_GET["id"]))
                ->find();
        if ($photo->id) {
            $photo->delete();
        }
        $this->finish();
    }

    public function action_clear_file() {
        $this->auto_render = false;
        $one = $_GET["file"];
        $file = "files/$one";
        $file2 = "files/thumbnail/$one";
        @unlink($file);
        @unlink($file2);
        return;
    }

    public function action_save_texts($lang) {
        // $this->user->main_text = $_POST["main_text"];
        $this->user->about_us = $_POST["about_us"];
        $this->user->delivery_info = $_POST["delivery_info"];
        $this->user->contact_text = $_POST["contact_text"];
        // $this->user->contact_map = $_POST["contact_map"];
        //  $this->user->cart_text = $_POST["cart_text"];
        $this->user->save();
        die(json_encode(array("okay" => true)));
    }

    public function action_save_settings() {


        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('phone', 'phone')
                ->label('email', 'email');
        //->label('copy', 'copy');
        //  $valid->rule('copy', 'not_empty');

        $valid->rule('email', 'not_empty')
                ->rule('email', 'email');

        $valid->rule('phone', 'regex', array("/(^\+(\s?)(\d(\s?))+)$|(^(\d(\s?))+)$/"))
                ->rule('phone', 'min_length', array(6))
                ->rule('phone', 'not_empty')
                ->rule('phone', 'max_length', array(15));


        $valid_form = $valid->check();
        if ($valid_form) {
            $this->user->settings_email = $_POST["email"];
            $this->user->settings_phone = $_POST["phone"];
            $this->user->settings_copy = $_POST["copy"];
            $this->user->contact_map = $_POST["contact_map"];
            $this->user->main_text = $_POST["main_text"];
            $this->user->cart_text = $_POST["cart_text"];
            $this->user->js = $_POST["js"];

            $this->user->settings_vk = $_POST["settings_vk"];
            $this->user->settings_fb = $_POST["settings_fb"];
            $this->user->settings_od = $_POST["settings_od"];
            $this->user->settings_gp = $_POST["settings_gp"];

            $this->user->breadcrumb = $_POST["bc"];
            $this->user->seo_title = $_POST["seo_title"];
            $this->user->seo_keywords = $_POST["seo_keywords"];
            $this->user->seo_description = $_POST["seo_description"];
            $this->user->robots = $_POST["robots"];
            $this->user->use_robots = ($_POST["use_robots"] == "on") ? 1 : 0;
            $this->user->use_sitemap = ($_POST["use_sitemap"] == "on") ? 1 : 0;

            $this->user->shop_seo_keywords = $_POST["shop_seo_keywords"];
            $this->user->shop_seo_title = $_POST["shop_seo_title"];
            $this->user->shop_seo_description = $_POST["shop_seo_description"];

            //$this->user->theme_id = $_POST["theme_id"];
            if (trim($_POST['hfavico'])) {
                $ava = trim($_POST['hfavico']);
                $file = 'uploads/users/' . $ava;
                if (file_exists($file)) {
                    if ($this->user->favico) {
                        @unlink("uploads/settings/" . $this->user->favico);
                    }
                    $newfile = "uploads/settings/" . $ava;
                    copy($file, $newfile);
                    @unlink($file);
                    $this->user->favico = $ava;
                }
            }


            if (trim($_POST['hsettings_logo'])) {
                $ava = trim($_POST['hsettings_logo']);
                $file = 'uploads/users/' . $ava;
                if (file_exists($file)) {
                    if ($this->user->settings_logo) {
                        @unlink("uploads/settings/" . $this->user->settings_logo);
                    }

                    $image = Image::factory($file);
                    if ($image->width > 500)
                        $image->resize(500, null);
                    if ($imahe->height > 500)
                        $image->resize(null, 500);
                    $result = $image->save("uploads/settings/" . $ava);
                    //   copy($file, $newfile);
                    @unlink($file);
                    $this->user->settings_logo = $ava;
                }
            }

            if (trim($_POST['hsettings_visitka'])) {
                $ava = trim($_POST['hsettings_visitka']);
                $file = 'uploads/users/' . $ava;
                if (file_exists($file)) {
                    if ($this->user->settings_visitka) {
                        @unlink("uploads/settings/" . $this->user->settings_visitka);
                    }

                    $image = Image::factory($file);
                    if ($this->user->ads_type_id == 3) {
                        $image->resize(220, 220, Image::NONE);
                    } elseif ($this->user->ads_type_id == 2) {
                        $image->resize(340, 220, Image::NONE);
                    } elseif ($this->user->ads_type_id == 1) {
                        $image->resize(700, 220, Image::NONE);
                    } else {
                        $image->resize(220, 220, Image::NONE);
                    }
                    $result = $image->save("uploads/settings/" . $ava);
                    //   copy($file, $newfile);
                    @unlink($file);
                    $this->user->settings_visitka = $ava;
                }
            }
            $this->user->save();

            //-----------TAB3
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
        //   }
        //  else
        //    die(json_encode(array("reload" => true)));

        die(json_encode(array("okay" => true)));
    }

    public function action_save_banner() {
        $id = (int) $_POST["pid"];

        $arr = orm::factory("banner")->one($this->user->id, $id);

        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('name', 'name')
                ->label('href', 'href');

        $valid->rule('name', 'not_empty')
                ->rule('name', 'min_length', array(2))
                ->rule('name', 'max_length', array(100));
        $valid->rule('href', 'url');


        $valid_form = $valid->check();
        if ($valid_form) {

            //-----------TAB1
            $arr->name = trim($_POST["name"]);
            $arr->order = (int) trim($_POST["order"]);
            $arr->watch = $_POST["watch"];
            //-----------TAB2
            if (trim($_POST['hmain_img'])) {
                $ava = trim($_POST['hmain_img']);

                $file = 'uploads/users/' . $ava;
                if (file_exists($file)) {
                    if ($arr->img) {
                        @unlink("uploads/settings/" . $arr->img);
                    }
                    $image = Image::factory($file);
                    $newfile = 'uploads/settings/' . $ava;
                    $image->resize(940, 390, Image::NONE);
                    $result = $image->save($newfile);
                    //   copy($file, $newfile);
                    @unlink($file);
                    $arr->img = $ava;
                }
            }
            //-----------TAB3
            $arr->href = $_POST["href"];
            $arr->save();
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
        //   }
        //  else
        //    die(json_encode(array("reload" => true)));

        die(json_encode(array("id" => $arr->id)));
    }

    public function action_save_text($lang, $root = 1) {
        $name = $lang . "_name";
        $short_text = $lang . "_short_text";
        $text = $lang . "_text";
        $seo_title = $lang . "_seo_title";
        $seo_description = $lang . "_seo_description";
        $seo_keywords = $lang . "_seo_keywords";
        $id = (int) $_POST["pid"];

        $arr = orm::factory("pub")->where("id", "=", $id)->find();

        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('name', 'name')
                ->label('name_trans', 'name_trans');

        $valid->rule('name', 'not_empty')
                ->rule('name', 'min_length', array(2))
                ->rule('name', 'max_length', array(100));
        //$valid->rule('href', 'url');


        $valid_form = $valid->check();
        if ($valid_form) {

            //-----------TAB1
            $arr->name = trim($_POST["name"]);
            $arr->$name = trim($_POST["$name"]);
            $arr->name_trans = trim($_POST["name_trans"]);
            $arr->$short_text = trim($_POST["$short_text"]);
            $arr->$text = trim($_POST["$text"]);
            $arr->order = (int) trim($_POST["order"]);
            $arr->watch = $_POST["watch"];
            $arr->root = 5;
            $arr->up = ($_POST["up"] == 1) ? 1 : 0;
            $arr->bottom = ($_POST["bottom"] == 1) ? 1 : 0;
            //-----------TAB3
            $arr->$seo_title = $_POST["$seo_title"];
            $arr->$seo_description = $_POST["$seo_description"];
            $arr->$seo_keywords = $_POST["$seo_keywords"];
            foreach ($this->trans as $one) {
                $text = $one->name . "_text";
                $short_text = $one->name . "_short_text";
                $name = $one->name . "_name";
                $seo_title = $one->name . "_seo_title";
                $seo_keywords = $one->name . "_seo_keywords";
                $seo_description = $one->name . "_seo_description";
                $arr->$text = trim($_POST["$text"]);
                $arr->$short_text = trim($_POST["$short_text"]);
                $arr->$name = trim($_POST["$name"]);
                $arr->$seo_title = trim($_POST["$seo_title"]);
                $arr->$seo_keywords = trim($_POST["$seo_keywords"]);
                $arr->$seo_description = trim($_POST["$seo_description"]);
            }
            $arr->save();
            if ($arr->id) {
                $time = time();
                foreach ($_POST["photos"] as $key => $one) {
                    $file = explode("files", $one);
                    $file = "files/" . $file[1];
                    $file2 = "files/thumbnail/" . $file[1];
                    if (file_exists($file)) {
                        $image = Image::factory($file);
                        $watermark = orm::factory("static")->get("watermark")->get_one_value();
                        if ($watermark->id) {
                            $BASIC_IMG_H = $image->height;
                            $BASIC_IMG_W = $image->width;
                            $watermark = self::watermark('uploads/settings/' . $watermark->img, $BASIC_IMG_W, $BASIC_IMG_H, 1);
                            $pos_x = ($BASIC_IMG_H / 2) - ($watermark->height / 2);
                            $pos_y = ($BASIC_IMG_W / 2) - ($watermark->width / 2);
                            $result = $image->watermark($watermark, $pos_y, $pos_x);
                        }
                        if ($image->width > 1000) {
                            $image->resize(1000, null);
                        }
                        if ($image->height > 1000) {
                            $image->resize(null, 1000);
                        }
                        $na = explode("/", $one);
                        $name = $na[count($na) - 1];

                        $na = explode(".", $name);
                        $name1 = $na[count($na) - 2];
                        $image->save("uploads/photos/img/$time" . $name);


                        if ($image->width > 240) {
                            $image->resize(240, null);
                        }
                        if ($image->height > 240) {
                            $image->resize(null, 240);
                        }
                        $image->save("uploads/photos/thumb/$time" . $name);
                        $ph = orm::factory("photo");
                        $ph->name = $name;
                        foreach ($this->trans as $one) {
                            $name1 = $one->name . "_name";
                            $ph->$name1 = $name;
                        }
                        $ph->img = $time . $name;
                        $ph->watch = 1;
                        $ph->save();

                        @unlink($file);
                        @unlink($file2);
                        $rel = orm::factory("pubs_photo");
                        $rel->pub_id = $arr->id;
                        $rel->photo_id = $ph->id;
                        $rel->save();
                    }
                }
            }
            if (!$_POST["menu_id"])
                $arr->root = $root;
            else
                $arr->root = 0;
            $arr->save();

            if ($_POST["menu_id"]) {
                $par = orm::factory("pub")->where("id", "=", $_POST["menu_id"])->find();
                if (!$par->id) {
                    $query = DB::delete('pubs_pubs')
                            // ->where('eshop_menu_id', "=", $par->id)
                            ->where('next_pub_id', "=", $arr->id)
                            ->execute();
                } else {
                    $rel = orm::factory('pubs_pub')
                            ->where('pub_id', "=", $par->id)
                            ->and_where('next_pub_id', "=", $arr->id)
                            ->find();
                    if (!$rel->next_pub_id) {
                        $query = DB::delete('pubs_pubs')
                                ->where('next_pub_id', "=", $arr->id)
                                ->execute();
                        $rel->pub_id = $par->id;
                        $rel->next_pub_id = $arr->id;
                        $rel->save();
                        // $rel->order = $rel->id;
                        // $rel->save();
                    }
                    $arr->root = 0;
                    $arr->save();
                }
            } else {
                $query = DB::delete('pubs_pubs')
                        ->where('next_pub_id', "=", $arr->id)
                        ->execute();
            }
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
        //   }
        //  else
        //    die(json_encode(array("reload" => true)));

        die(json_encode(array("id" => $arr->id)));
    }

    public function action_save_widget() {
        $id = (int) $_POST["pid"];

        $arr = orm::factory("election")->one($this->user->id, $id);

        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('name', 'name')
                ->label('name_trans', 'name_trans');

        $valid->rule('name', 'not_empty')
                ->rule('name', 'min_length', array(2))
                ->rule('name', 'max_length', array(100));
        //$valid->rule('href', 'url');


        $valid_form = $valid->check();
        if ($valid_form) {
            $arr->user_id = $this->user->id;

            //-----------TAB1
            $arr->name = trim($_POST["name"]);
            // $arr->text = trim($_POST["text"]);
            $arr->order = (int) trim($_POST["order"]);
            $arr->watch = $_POST["watch"];
            // $arr->root = 5;
            //  $arr->up = ($_POST["up"] == 1) ? 1 : 0;
            //  $arr->bottom = ($_POST["bottom"] == 1) ? 1 : 0;
            //-----------TAB3
            //  $arr->seo_title = $_POST["seo_title"];
            //  $arr->seo_description = $_POST["seo_description"];
            //  $arr->seo_keywords = $_POST["seo_keywords"];
            $arr->save();
            $arr->remove_all("products");
            foreach ($_POST["elp"] as $item) {
                $ar1 = ORM::factory("product")->where("id", "=", $item)->find();
                $ar1->add("elections", $arr);
            }
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
        //   }
        //  else
        //    die(json_encode(array("reload" => true)));

        die(json_encode(array("id" => $arr->id)));
    }

    public function action_save_order($lang) {
        $id = (int) $_POST["pid"];

        $arr = orm::factory("order")->where("id", "=", $id)->find();
        if (!$arr->id) {
            die(json_encode(array("relode" => true)));
        }
        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('name', 'name')
                ->label('name_trans', 'name_trans');

        $valid->rule('name', 'not_empty')
                ->rule('name', 'min_length', array(2))
                ->rule('name', 'max_length', array(100));
        //$valid->rule('href', 'url');


        $valid_form = $valid->check();
        if ($valid_form) {
            $p1 = $arr->payment_method_id;
            $p2 = $arr->payu_method_id;
            //-----------TAB1
            $arr->name = trim($_POST["name"]);
            $arr->surname = trim($_POST["surname"]);
            $arr->post_n = trim($_POST["post_n"]);
            // $arr->text = trim($_POST["text"]);
            //-----------TAB3
            $arr->code = $_POST["code"];
            $arr->email = $_POST["email"];
            $arr->phone = $_POST["phone"];
            $arr->adress = $_POST["adress"];
            $arr->street = $_POST["street"];
            $arr->city = $_POST["city"];
            $arr->house = $_POST["house"];
            $arr->flat = $_POST["flat"];
            $arr->user_comment = $_POST["user_comment"];
            $arr->payment_method_id = $_POST["payment_method_id"];
            $arr->payu_method_id = $_POST["payu_method_id"];
            $arr->orders_status_id = $_POST["orders_status_id"];
            $arr->save();
            if ($p1 != $arr->payment_method_id) {
                $arr->payment_name = $arr->payment_method->name;
                $arr->payment_price = $arr->payment_method->price;
                $arr->save();
            }
            if ($p2 != $arr->payu_method_id) {
                $arr->payu_name = $arr->payu_method->name;
                $arr->payu_price = $arr->payu_method->price;
                $arr->save();
            }
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
        //   }
        //  else
        //    die(json_encode(array("reload" => true)));

        die(json_encode(array("id" => $arr->id)));
    }

    public function action_save_pub($lang) {
        $name = $lang . "_name";
        $short_text = $lang . "_short_text";
        $text = $lang . "_text";
        $alt = $lang . "_alt";
        $seo_title = $lang . "_seo_title";
        $seo_description = $lang . "_seo_description";
        $seo_keywords = $lang . "_seo_keywords";
        $id = (int) $_POST["pid"];

        $arr = orm::factory("text")->where("id", "=", $id)->find();

        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('name', 'name')
                ->label('name_trans', 'name_trans');

        $valid->rule('name', 'not_empty')
                ->rule('name', 'min_length', array(2))
                ->rule('name', 'max_length', array(100));
        //$valid->rule('href', 'url');


        $valid_form = $valid->check();
        if ($valid_form) {
            $tp = orm::factory("texts_cat")
                    ->where("id", "=", $_POST["texts_type_id"])
                    ->find();
            //-----------TAB1
            $arr->$alt = trim($_POST["$alt"]);
            $arr->name = trim($_POST["name"]);
            $arr->source = trim($_POST["source"]);
            $arr->$name = trim($_POST["$name"]);
            $arr->name_trans = trim($_POST["name_trans"]);
            $arr->$text = trim($_POST["$text"]);
            $arr->$short_text = trim($_POST["$short_text"]);
            $arr->order = (int) trim($_POST["order"]);
            $arr->texts_cat_id = (int) trim($_POST["texts_type_id"]);

            $arr->watch = $_POST["watch"];
            $arr->is_sold = $_POST["is_sold"];

            //-----------TAB2
            if (trim($_POST['hmain_img'])) {
                $ava = trim($_POST['hmain_img']);

                $file = 'uploads/users/' . $ava;
                if (file_exists($file)) {
                    $newfile1 = 'uploads/pubs/' . $ava;
                    $newfile2 = 'uploads/pubs/thumb/' . $ava;

                    if ($arr->img) {
                        @unlink("uploads/pubs/" . $arr->img);
                        @unlink("uploads/pubs/thumb/" . $arr->img);
                    }
                    $image = Image::factory($file);
                    $newfile = 'uploads/settings/' . $ava;
                    $image->resize($tp->width, $tp->height);
                    $result = $image->save($newfile1);
                    $result = $image->save($newfile2);
                    //   copy($file, $newfile);
                    @unlink($file);
                    $arr->img = $ava;
                }
            }
            //logo
            if (trim($_POST['hmain_img_logo'])) {
                $ava = trim($_POST['hmain_img_logo']);

                $file = 'uploads/users/' . $ava;
                if (file_exists($file)) {
                    $newfile1 = 'uploads/pubs/' . $ava;
                    $newfile2 = 'uploads/pubs/thumb/' . $ava;

                    if ($arr->logo) {
                        @unlink("uploads/pubs/" . $arr->logo);
                        @unlink("uploads/pubs/thumb/" . $arr->logo);
                    }
                    $image = Image::factory($file);
                    $newfile = 'uploads/settings/' . $ava;
                    $image->resize($tp->width, $tp->height);
                    $result = $image->save($newfile1);
                    $result = $image->save($newfile2);
                    //   copy($file, $newfile);
                    @unlink($file);
                    $arr->logo = $ava;
                }
            }
            foreach ($this->trans as $one) {
                $text = $one->name . "_text";
                $alt = $one->name . "_alt";
                $short_text = $one->name . "_short_text";
                $name = $one->name . "_name";
                $seo_title = $one->name . "_seo_title";
                $seo_keywords = $one->name . "_seo_keywords";
                $seo_description = $one->name . "_seo_description";
                $arr->$alt = trim($_POST["$alt"]);
                $arr->$text = trim($_POST["$text"]);
                $arr->$short_text = trim($_POST["$short_text"]);
                $arr->$name = trim($_POST["$name"]);
                $arr->$seo_title = trim($_POST["$seo_title"]);
                $arr->$seo_keywords = trim($_POST["$seo_keywords"]);
                $arr->$seo_description = trim($_POST["$seo_description"]);
            }
            //-----------TAB3
            $arr->$seo_title = $_POST["$seo_title"];
            $arr->$seo_description = $_POST["$seo_description"];
            $arr->$seo_keywords = $_POST["$seo_keywords"];
            $arr->save();
            if ($arr->id) {
                $time = time();
                foreach ($_POST["photos"] as $key => $one) {
                    $file = explode("files", $one);
                    $file = "files/" . $file[1];
                    $file2 = "files/thumbnail/" . $file[1];
                    if (file_exists($file)) {
                        $image = Image::factory($file);
                        $watermark = orm::factory("static")->get("watermark")->get_one_value();
                        if ($watermark->id) {
                            $BASIC_IMG_H = $image->height;
                            $BASIC_IMG_W = $image->width;
                            $watermark = self::watermark('uploads/settings/' . $watermark->img, $BASIC_IMG_W, $BASIC_IMG_H, 1);
                            $pos_x = ($BASIC_IMG_H / 2) - ($watermark->height / 2);
                            $pos_y = ($BASIC_IMG_W / 2) - ($watermark->width / 2);
                            $result = $image->watermark($watermark, $pos_y, $pos_x);
                        }
                        if ($image->width > 1000) {
                            $image->resize(1000, null);
                        }
                        //if ($image->height > 1000) {
                        //$image->resize(null, 1000);
                        //}
                        $na = explode("/", $one);
                        $name = $na[count($na) - 1];

                        $na = explode(".", $name);
                        $name1 = $na[count($na) - 2];
                        $image->save("uploads/photos/img/$time" . $name);


                        if ($image->width > 240) {
                            $image->resize(240, null);
                        }
                        if ($image->height > 240) {
                            $image->resize(null, 240);
                        }
                        $image->save("uploads/photos/thumb/$time" . $name);
                        $ph = orm::factory("photo");
                        $ph->name = $name;
                        foreach ($this->trans as $one) {
                            $name1 = $one->name . "_name";
                            $ph->$name1 = $name;
                        }
                        $ph->img = $time . $name;
                        $ph->watch = 1;
                        $ph->save();

                        @unlink($file);
                        @unlink($file2);
                        $rel = orm::factory("texts_photo");
                        $rel->text_id = $arr->id;
                        $rel->photo_id = $ph->id;
                        $rel->save();
                    }
                }
            }
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
        //   }
        //  else
        //    die(json_encode(array("reload" => true)));

        die(json_encode(array(
            "id" => $arr->id,
            "redirect" => "/" . $lang . "/admin/office/publications/" . $_POST["texts_type_id"]
        )));
    }

    public function action_delete_banner() {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("banner")->where("id", "=", $id)->find();
            if ($arr->id) {
                @unlink("uploads/settings/" . $arr->img);
                $arr->delete();
                die(json_encode(array("okay" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_delete_order_product() {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("orders_product")->where("id", "=", $id)->find();
            if ($arr->id) {
                $arr->delete();
                die(json_encode(array("okay" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_delete_pub() {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("text")->where("id", "=", $id)->find();
            if ($arr->id) {
                @unlink("uploads/docs/" . $arr->img);
                @unlink("uploads/docs/thumb/" . $arr->img);

                $arr->delete();
                die(json_encode(array("okay" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_delete_menu($lang) {
        $id = arr::get($_POST, "id");
        if ($id) {
            //self::delete_menu_rec($id);
            $arr = ORM::factory("pub")->where("id", "=", $id)->find();
            if ($arr->controller != "" || $arr->locked == "1") {
                $this->finish('error', __("system_menu_forbiden_to_delete"));
            }
            $childs = $arr->get_menu_adm();
            if (count($childs) > 0) {
                $this->finish('error', __("menus_has_childs"));
            }
            if ($arr->id) {
                $arr->delete();
                die(json_encode(array("okay" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    function delete_menu_rec($id) {
        $arr = ORM::factory("pub")->where("id", "=", $id)->find();
        if ($arr->id) {
            $childs = $arr->get_menu_adm();
            $arr->delete();
            foreach ($childs as $one) {
                self::delete_menu_rec($one->id);
            }
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_toggle_banner_activity() {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("banner")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->watch == 0) {
                    $arr->watch = 1;
                    $title = __("turn_off");
                } else {
                    $arr->watch = 0;
                    $title = __("turn_on");
                }
                $arr->save();
                die(json_encode(array("watch" => $arr->watch, "title" => $title)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_toggle_pub_activity() {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("text")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->watch == 0) {
                    $arr->watch = 1;
                    $title = __("turn_off");
                } else {
                    $arr->watch = 0;
                    $title = __("turn_on");
                }
                $arr->save();
                die(json_encode(array("watch" => $arr->watch, "title" => $title)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_toggle_menu_activity($lang) {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("pub")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->watch == 0) {
                    $arr->watch = 1;
                    $title = __("turn_off");
                } else {
                    $arr->watch = 0;
                    $title = __("turn_on");
                }
                $arr->save();
                die(json_encode(array("watch" => $arr->watch, "title" => $title)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_toggle_widget_activity() {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("election")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->watch == 0) {
                    $arr->watch = 1;
                    $title = __("turn_off");
                } else {
                    $arr->watch = 0;
                    $title = __("turn_on");
                }
                $arr->save();
                die(json_encode(array("watch" => $arr->watch, "title" => $title)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_delete_widget() {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("election")->where("id", "=", $id)->find();
            if ($arr->id) {
                $query = DB::delete('elections_products')
                        // ->where('eshop_menu_id', "=", $par->id)
                        ->where('election_id', "=", $arr->id)
                        ->execute();
                $arr->delete();
                die(json_encode(array("okay" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_save_pay($lang) {
        $name = $lang . "_name";
        $text = $lang . "_text";
        $id = (int) $_POST["pid"];
        $arr = ORM::factory("payu_method")->where("id", "=", $id)->find();
        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('name', 'name')
                ->label('name_trans', 'name_trans');
        $valid->rule('name', 'not_empty')
                ->rule('name', 'min_length', array(2))
                ->rule('name', 'max_length', array(100));
        $valid_form = $valid->check();
        if ($valid_form) {
            $arr->user_id = $this->user->id;
            //-----------TAB1
            $arr->name = trim($_POST["name"]);
            $arr->$name = trim($_POST["$name"]);
            $arr->$text = trim($_POST["$text"]);
            $arr->price = trim($_POST["price"]);
            $arr->order = (int) trim($_POST["order"]);
            $arr->watch = $_POST["watch"];
            $arr->save();
            die(json_encode(array("id" => $arr->id)));
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
    }

    public function action_toggle_pay_activity($lang) {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("payu_method")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->watch == 0) {
                    $arr->watch = 1;
                    $title = __("turn_off");
                } else {
                    $arr->watch = 0;
                    $title = __("turn_on");
                }
                $arr->save();
                die(json_encode(array("watch" => $arr->watch, "title" => $title)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_delete_pay($lang) {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("payu_method")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->delete())
                    die(json_encode(array("okay" => true)));
                else
                    die(json_encode(array("reload" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    /*
     * saves category
     */

    public function action_save_category($lang) {
        $name = $lang . "_name";
        $short_text = $lang . "_short_text";
        $text = $lang . "_text";
        $seo_title = $lang . "_seo_title";
        $seo_description = $lang . "_seo_description";
        $seo_keywords = $lang . "_seo_keywords";
        $id = (int) $_POST["pid"];

        $arr = ORM::factory("category")->where("id", "=", $id)->find();

        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('name', 'name')
                ->label('name_trans', 'name_trans');

        $valid->rule('name', 'not_empty')
                ->rule('name', 'min_length', array(2))
                ->rule('name', 'max_length', array(100));
        $valid->rule('name_trans', 'not_empty')
                ->rule('name_trans', 'min_length', array(2))
                ->rule('name_trans', 'max_length', array(100))
                ->rule('name_trans', 'Model_Product::check_name_trans', array($arr->id))
                ->rule('name_trans', 'alpha_dash');


        $valid_form = $valid->check();
        if ($valid_form) {

            //-----------TAB1
            $arr->name = trim($_POST["name"]);
            $arr->$name = trim($_POST["$name"]);
            $arr->name_trans = trim($_POST["name_trans"]);
            $arr->$text = trim($_POST["$text"]);
            $arr->$short_text = trim($_POST["$short_text"]);
            $arr->order = (int) trim($_POST["order"]);
            $arr->watch = $_POST["watch"];
            //-----------TAB2
            if (trim($_POST['hmain_img'])) {
                $ava = trim($_POST['hmain_img']);
                $file = 'uploads/users/' . $ava;
                if (file_exists($file)) {
                    $ava = trim($_POST['hmain_img']);
                    if ($arr->img) {
                        @unlink("uploads/eshop/" . $arr->img);
                    }
                    $image = Image::factory($file);
                    $newfile = 'uploads/eshop/' . $ava;
                    $image->resize(280, null);
                    $result = $image->save($newfile);

                    @unlink($file);
                    $arr->img = $ava;
                }
            }
            foreach ($this->trans as $one) {
                $text = $one->name . "_text";
               // $alt = $one->name . "_alt";
                $short_text = $one->name . "_short_text";
                $name = $one->name . "_name";
                $seo_title = $one->name . "_seo_title";
                $seo_keywords = $one->name . "_seo_keywords";
                $seo_description = $one->name . "_seo_description";
               // $arr->$alt = trim($_POST["$alt"]);
                $arr->$text = trim($_POST["$text"]);
                $arr->$short_text = trim($_POST["$short_text"]);
                $arr->$name = trim($_POST["$name"]);
                $arr->$seo_title = trim($_POST["$seo_title"]);
                $arr->$seo_keywords = trim($_POST["$seo_keywords"]);
                $arr->$seo_description = trim($_POST["$seo_description"]);
            }
            //-----------TAB3
            $arr->$seo_title = $_POST["$seo_title"];
            $arr->$seo_description = $_POST["$seo_description"];
            $arr->$seo_keywords = $_POST["$seo_keywords"];
            if (!$_POST["category_id"])
                $arr->root = 1;
            else
                $arr->root = 0;
            $arr->save();

            if ($_POST["category_id"]) {
                $par = orm::factory("category")->where("id", "=", $_POST["category_id"])->find();
                if (!$par->id) {
                    $query = DB::delete('categories_categories')
                            // ->where('eshop_menu_id', "=", $par->id)
                            ->where('to_category_id', "=", $arr->id)
                            ->execute();
                } else {
                    $rel = orm::factory('categories_category')
                            ->where('category_id', "=", $par->id)
                            ->and_where('to_category_id', "=", $arr->id)
                            ->find();
                    if (!$rel->to_category_id) {
                        $query = DB::delete('categories_categories')
                                ->where('to_category_id', "=", $arr->id)
                                ->execute();
                        $rel->category_id = $par->id;
                        $rel->to_category_id = $arr->id;
                        $rel->save();
                        // $rel->order = $rel->id;
                        // $rel->save();
                    }
                    $arr->root = 0;
                    $arr->save();
                }
            } else {
                $query = DB::delete('categories_categories')
                        ->where('to_category_id', "=", $arr->id)
                        ->execute();
            }
            $engine = new Helper_Catalog_Scanner($arr, true); //generate breadcrumbs 

            if ($em->id == "dddd") {
                $query = DB::delete('eshop_menus_products')
                        ->where('product_id', "=", $arr->id)
                        ->execute();
                $rel = orm::factory("eshop_menus_product")
                        //     ->where("eshop_menu_id", "=", $em->id)
                        ->and_where("product_id", "=", $arr->id)
                        ->find();
                $rel->eshop_menu_id = $em->id;
                $rel->product_id = $arr->id;
                $rel->save();
                $engine = new Helper_Catalog_Scanner($arr); //generate breadcrumbs 
            }
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
        //   }
        //  else
        //    die(json_encode(array("reload" => true)));

        die(json_encode(array("id" => $arr->id)));
    }

    public function action_activate_theme() {
        $id = arr::get($_POST, "id");

        $arr = ORM::factory("theme")->where("id", "=", $id)->find();
        if ($arr->id) {
            $this->user->theme_id = $arr->id;
            $this->user->save();
            die(json_encode(array("okay" => true)));
        } else {
            $this->user->theme_id = 0;
            $this->user->save();
            die(json_encode(array("okay" => true)));
        }
    }

    public function action_save_prices() {


        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('percent', 'percent');
        //->label('copy', 'copy');
        //  $valid->rule('copy', 'not_empty');

        $valid->rule('percent', 'not_empty')
                ->rule('percent', 'range', array(-100, 100));




        $valid_form = $valid->check();
        if ($valid_form) {
            $prods = orm::factory("product")
                    ->find_all();
            foreach ($prods as $one) {
                $one->price = round($one->price + $one->price * ($_POST["percent"] / 100));
                $one->save();
            }
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
        //   }
        //  else
        //    die(json_encode(array("reload" => true)));

        die(json_encode(array("msg" => __("prices_changed") . " " . $_POST["percent"] . " %")));
    }

    public function action_save_currency($lang) {
        $id = (int) $_POST["pid"];

        $arr = orm::factory("currency")->where("id", "=", $id)->find();

        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('name', 'name')
                ->label('href', 'href');

        $valid->rule('name', 'not_empty')
                ->rule('name', 'min_length', array(2))
                ->rule('name', 'max_length', array(100));



        $valid_form = $valid->check();
        if ($valid_form) {

            //-----------TAB1
            $arr->name = trim($_POST["name"]);
            $arr->order = (int) trim($_POST["order"]);
            $arr->watch = $_POST["watch"];
            //-----------TAB2
            //-----------TAB3
            $arr->code = $_POST["code"];
            $arr->date = date("Y-m-d");
            $arr->symbol1 = $_POST["symbol1"];
            $arr->symbol2 = $_POST["symbol2"];
            $arr->value = $_POST["value"];
            $arr->save();
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
        //   }
        //  else
        //    die(json_encode(array("reload" => true)));

        die(json_encode(array("id" => $arr->id)));
    }

    public function action_set_main_cur() {

        $cid = $_POST["cid"];
        $arr = orm::factory("currency")->where("id", "=", $cid)->find();
        if (!$arr->id) {
            die(json_encode(array("reload" => true)));
        }
        $query = DB::update('currencies')
                ->set(array('is_main' => 0))
                ->execute();
        $arr->is_main = 1;
        $arr->value = 1;
        $arr->save();
        die(json_encode(array("reload" => true)));
    }

    public function action_refresh_prices() {


        // if ($this->user->id == 15 || $this->user->id == 36) {
        $cur = orm::factory("currency")
                ->and_where("is_main", "=", 1)
                ->find();
        if (!$cur->id)
            die(json_encode(array("msg" => __("select_def_curr"))));
        $p = orm::factory("product")
                ->and_where("price_base", ">", 0)
                ->and_where("currency_id", ">", 0)
                // ->and_where("currency_id", "!=", $cur->id)
                ->find_all();
        foreach ($p as $one) {
            $c = orm::factory("currency")
                    ->and_where("id", "=", $one->currency_id)
                    ->find();
            if ($c->id) {
                $b = $one->price_base;
                $one->price = round($b * $c->value);
                $one->save();
            }
        }
        // } else {
        //      die(json_encode(array("msg" => "Модуль в разработке")));
        //  }
        die(json_encode(array("reload" => true)));
    }

    /*
     * delete parameter
     */

    public function action_delete_paramscat($lang) {
        $this->auto_render = false;
        $p = orm::factory("parameter")
                ->where("id", "=", $_POST["id"])
                ->find();
        if (!$p->id) {
            die(json_encode(array("reload" => true)));
        }
        //delete parameters_values
        $query = DB::delete('parameters_values')
                ->where('parameter_id', "=", $p->id)
                ->execute();
        //delete products_parameters
        $query = DB::delete('products_parameters')
                ->where('parameter_id', "=", $p->id)
                ->execute();
        $p->delete();
        die(json_encode(array("okay" => true)));
    }

    /*
     * delete attribute
     */

    public function action_delete_attribute($lang) {
        $this->auto_render = false;
        $p = orm::factory("attribute")
                ->where("id", "=", $_POST["id"])
                ->find();
        if (!$p->id) {
            die(json_encode(array("reload" => true)));
        }
        //delete parameters_values
        $query = DB::delete('attributes_values')
                ->where('attribute_id', "=", $p->id)
                ->execute();
        //delete products_parameters
        $query = DB::delete('products_attributes')
                ->where('attribute_id', "=", $p->id)
                ->execute();
        $p->delete();
        die(json_encode(array("okay" => true)));
    }

    /*
     * delete parameters_value
     */

    public function action_delete_param($lang) {
        $this->auto_render = false;
        $p = orm::factory("parameters_value")
                ->where("id", "=", $_POST["id"])
                ->find();
        if (!$p->id) {
            die(json_encode(array("reload" => true)));
        }

        //delete products_parameters
        $query = DB::delete('products_parameters')
                ->where('parameters_value_id', "=", $p->id)
                ->execute();
        $p->delete();
        die(json_encode(array("okay" => true)));
    }

    /*
     * Saves settings value
     */

    public function action_save_sv($lang) {
        $name = $lang . "_name";
        $href = $lang . "_href";
        $value = $lang . "_value";
        $lvalue = $lang . "_lvalue";
        $short_text = $lang . "_short_text";
        $text = $lang . "_text";
        $alt = $lang . "_alt";
        $seo_title = $lang . "_seo_title";
        $seo_description = $lang . "_seo_description";
        $seo_keywords = $lang . "_seo_keywords";
        $id = (int) $_POST["pid"];


        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('value', 'value');

        $valid->rule('value', 'not_empty')
                ->rule('value', 'min_length', array(1))
                ->rule('value', 'max_length', array(100));
        //$valid->rule('href', 'url');


        $valid_form = $valid->check();
        if ($valid_form) {
            $arr = orm::factory("statics_data")
                    ->where("id", "=", $id)
                    ->find();
            $sn = orm::factory("static")
                    ->where("id", "=", $_POST["settings_name_id"])
                    ->find();
            if (!$arr->id && !$sn->id) {
                die(json_encode(array("url" => url::base() . "$lang/admin/office/statics")));
            }
            if ($sn->id)
                $arr->static_id = $sn->id;

            //-----------TAB1
            $arr->value = trim($_POST["value"]);
            $arr->$lvalue = trim($_POST["$lvalue"]);
            $arr->target = trim($_POST["target"]);
            $arr->$value = trim($_POST["$value"]);
            $arr->$alt = trim($_POST["$alt"]);
            $arr->$href = trim($_POST["$href"]);
            $arr->$text = trim($_POST["$text"]);
            $arr->$short_text = trim($_POST["$short_text"]);
            if (trim($_POST['hmain_img'])) {
                $ava = trim($_POST['hmain_img']);

                $file = 'uploads/users/' . $ava;
                if (file_exists($file)) {
                    $newfile1 = 'uploads/static/' . $ava;
                    $newfile2 = 'uploads/static/thumb/' . $ava;

                    if ($arr->img) {
                        @unlink("uploads/static/" . $arr->img);
                        @unlink("uploads/static/thumb/" . $arr->img);
                    }
                    $image = Image::factory($file);
                    $newfile = 'uploads/static/' . $ava;
                    if ($image->width > 1366)
                        $image->resize(1366, null);
                    $result = $image->save($newfile1);
                    //    if($sn->img_width)
                    $image->resize($sn->img_width, $sn->img_height);
                    $result = $image->save($newfile2);
                    //   copy($file, $newfile);
                    @unlink($file);
                    $arr->img = $ava;
                }
            }
            //   $arr->name_trans = trim($_POST["name_trans"]);
            //   $arr->$text = trim($_POST["$text"]);
            //  $arr->$short_text = trim($_POST["$short_text"]);
            $arr->order = (int) trim($_POST["order"]);
            $arr->watch = $_POST["watch"];
            //-----------TAB2
            if (trim($_POST['hmain_img'])) {
                $ava = trim($_POST['hmain_img']);

                $file = 'uploads/users/' . $ava;
                if (file_exists($file)) {
                    $newfile1 = 'uploads/docs/' . $ava;
                    $newfile2 = 'uploads/docs/thumb/' . $ava;

                    if ($arr->img) {
                        @unlink("uploads/dosc/" . $arr->img);
                        @unlink("uploads/dosc/thumb/" . $arr->img);
                    }
                    $image = Image::factory($file);
                    $newfile = 'uploads/settings/' . $ava;
                    $image->resize(287, 121);
                    $result = $image->save($newfile1);
                    $result = $image->save($newfile2);
                    //   copy($file, $newfile);
                    @unlink($file);
                    $arr->img = $ava;
                }
            }
            //-----------TAB3
            //localisations
            foreach ($this->trans as $one) {
                $text = $one->name . "_text";
                $short_text = $one->name . "_short_text";
                $href = $one->name . "_href";
                $value = $one->name . "_value";
                $lvalue = $one->name . "_lvalue";
                $alt = $one->name . "_alt";
                $arr->$alt = trim($_POST["$alt"]);
                $arr->$lvalue = trim($_POST["$lvalue"]);
                $arr->$value = trim($_POST["$value"]);
                $arr->$href = trim($_POST["$href"]);
                $arr->$text = trim($_POST["$text"]);
                $arr->$short_text = trim($_POST["$short_text"]);
            }
            $arr->save();
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }

        die(json_encode(array("id" => $arr->id, "pid" => $arr->static_id)));
    }

    public function action_delete_settings_value($lang) {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("statics_data")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->delete())
                    die(json_encode(array("okay" => true)));
                else
                    die(json_encode(array("reload" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    /*
     * removes settings img
     */

    public function action_remove_img_settings($lang) {
        $id = arr::get($_POST, "pid");
        if ($id) {
            $arr = ORM::factory("statics_data")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->img) {
                    @unlink("uploads/static/" . $arr->img);
                    @unlink("uploads/static/thumb/" . $arr->img);

                    $arr->img = "";
                    $arr->save();
                }
                die(json_encode(array("okay" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    /*
     * delete currency
     */

    public function action_delete_currency($lang) {
        $this->auto_render = false;
        $p = orm::factory("currency")
                ->where("id", "=", $_POST["id"])
                ->find();
        if (!$p->id) {
            die(json_encode(array("reload" => true)));
        }
        //delete currency
        $query = DB::delete('currencies')
                ->where('id', "=", $p->id)
                ->execute();

        $p->delete();
        die(json_encode(array("okay" => true)));
    }

    public function action_toggle_currency_activity() {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("currency")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->watch == 0) {
                    $arr->watch = 1;
                    $title = __("turn_off");
                } else {
                    $arr->watch = 0;
                    $title = __("turn_on");
                }
                $arr->save();
                die(json_encode(array("watch" => $arr->watch, "title" => $title)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    public function action_refresh_tree($lang) {
        $this->auto_render = false;
        $strRequest = file_get_contents('php://input');
        $request = json_decode($strRequest);
        $i = 1;
        DB::delete('categories_categories')
                ->execute();
        self::refresh_help($request, $i);

        /* foreach ($request as $one) {
          echo $one->id;
          print_r($one->children);
          echo "<br>---------------------<br>";
          } */
        die(json_encode(array("message" => __("admin_changes_saved"))));
    }

    function refresh_help($request, $i, $pid = false) {

        foreach ($request as $one) {
            // print_r($one);
            //  echo $one->id."wef";
            //  die();
            $i++;

            $id = $one->id;
            //  print_r($one->children);
            //  die();
            $em = orm::factory("category")
                    ->where("id", "=", $id)
                    ->find();
            if (!$em->id) {// no object - send relode command (maybe someone delete it recently)
                die(json_encode(array("reload" => true, "id" => $id)));
            }
            if ($pid) {/// if got father - clear the root 
                $em->root = 0;
            } else {//if becomes root clear father rels
                $em->root = 1;
                DB::delete('categories_categories')
                        ->where('to_category_id', "=", $id)
                        ->execute();
            }
            $em->order = $i;
            $em->save();
            if ($pid) { //check we have relation to given father, add if it's havent
                $rel = orm::factory("categories_category")
                        // ->where("eshop_menu_id", "=", $pid)
                        ->where("to_category_id", "=", $em->id)
                        ->find();

                if (!$rel->category_id) {
                    $rel->category_id = $pid;
                    $rel->to_category_id = $em->id;
                    $rel->save();
                }
            }

            if (count($one->children) > 0) {
                //  echo "next";
                // foreach ($one->children as $two) {
                //    print_r($two);
                self::refresh_help($one->children, $i, $em->id);
                //  }
            } else {/// no childs then delete exists
                DB::delete('categories_categories')
                        ->where('category_id', "=", $id)
                        ->execute();
            }
            //  $childs = $one->children;
        }
    }

    //refresh menu structure
    public function action_refresh_tree_menu($lang, $root) {
        $this->auto_render = false;
        $strRequest = file_get_contents('php://input');
        $request = json_decode($strRequest);
        $i = 1;
        $pid = false;
        DB::delete('pubs_pubs')
                ->execute();
        self::refresh_help_menu($request, $i, $pid, $root);

        /* foreach ($request as $one) {
          echo $one->id;
          print_r($one->children);
          echo "<br>---------------------<br>";
          } */
        die(json_encode(array("message" => __("admin_changes_saved"))));
    }

    function refresh_help_menu($request, $i, $pid = false, $root) {

        foreach ($request as $one) {
            // print_r($one);
            //  echo $one->id."wef";
            //  die();
            $i++;

            $id = $one->id;
            //  print_r($one->children);
            //  die();
            $em = orm::factory("pub")
                    ->where("id", "=", $id)
                    ->find();
            if (!$em->id) {// no object - send relode command (maybe someone delete it recently)
                die(json_encode(array("reload" => true, "id" => $id)));
            }
            if ($pid) {/// if got father - clear the root 
                $em->root = 0;
            } else {//if becomes root clear father rels
                $em->root = $root;
                DB::delete('pubs_pubs')
                        ->where('next_pub_id', "=", $id)
                        ->execute();
            }
            $em->order = $i;
            $em->save();
            if ($pid) { //check we have relation to given father, add if it's havent
                $rel = orm::factory("pubs_pub")
                        // ->where("eshop_menu_id", "=", $pid)
                        ->where("next_pub_id", "=", $em->id)
                        ->find();

                if (!$rel->pub_id) {
                    $rel->pub_id = $pid;
                    $rel->next_pub_id = $em->id;
                    $rel->save();
                }
            }

            if (count($one->children) > 0) {
                //  echo "next";
                // foreach ($one->children as $two) {
                //    print_r($two);
                self::refresh_help_menu($one->children, $i, $em->id, $root);
                //  }
            } else {/// no childs then delete exists
                DB::delete('pubs_pubs')
                        ->where('pub_id', "=", $id)
                        ->execute();
            }
            //  $childs = $one->children;
        }
    }

    /*
     * deletes one orders status
     */

    public function action_delete_status($lang) {
        $id = arr::get($_POST, "id");
        if ($id) {
            $arr = ORM::factory("orders_status")->where("id", "=", $id)->find();
            if ($arr->id) {
                if ($arr->delete())
                    die(json_encode(array("okay" => true)));
                else
                    die(json_encode(array("reload" => true)));
            } else
                die(json_encode(array("reload" => true)));
        } else
            die(json_encode(array("reload" => true)));
    }

    /*
     * saves one orders status
     */

    public function action_save_status($lang) {
        $name = $lang . "_name";
        $text = $lang . "_text";
        $id = (int) $_POST["pid"];
        $arr = ORM::factory("orders_status")->where("id", "=", $id)->find();
        //  if ($_POST == Security::xss_clean($_POST)) {
        $valid = Validate::factory($_POST)
                ->label('name', 'name')
                ->label('name_trans', 'name_trans');
        $valid->rule('name', 'not_empty')
                ->rule('name', 'min_length', array(2))
                ->rule('name', 'max_length', array(100));
        $valid_form = $valid->check();
        if ($valid_form) {
            //-----------TAB1
            $arr->name = trim($_POST["name"]);
            $arr->$name = trim($_POST["$name"]);
            $arr->$text = trim($_POST["$text"]);
            $arr->class = trim($_POST["class"]);
            // $arr->price = trim($_POST["price"]);
            //  $arr->order = (int) trim($_POST["order"]);
            //  $arr->watch = $_POST["watch"];
            $arr->save();
            die(json_encode(array("id" => $arr->id)));
        } else {
            die(json_encode($valid->errors('contact_messages')));
        }
    }

}
