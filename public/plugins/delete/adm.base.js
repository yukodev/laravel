/* AJAX processing
 ajax answer format: answer[data, msgtype, msgcode]
 answer[data]    - returned data, no callback if === false, reload document if == 'reload'
 answer[msgtype] - type of message (success|notice|error)
 answer[msgtext] - message body
 */

$.ajaxSetup({// set default parameters for AJAX requests
    type: 'POST',
    dataType: 'json',
    cache: false,
    timeout: 35000
});

var ADM = {
    multilingual: true, // site uses a multilingual or not
    keepalive: 60     // sec, period of the keep alive ping
};

ADM.host = function () {
    return window.location.protocol + '//' + window.location.host;
}

ADM.lang = function () {
    return this.multilingual ? '/' + window.location.pathname.split('/', 2)[1] : '';
}

ADM.base_url = function () {
    return this.host() + this.lang();
}


/** Wrapper for AJAX requests
 *	Base parameters:
 *	url  string  Relative url to ajax file
 *	data  string  Data to be sent to the server
 *	callback  Callback function
 *	
 *	You freely can add any other parameters for $.ajax
 *	Usage example:
 *	ADM.sendData({
 *		async    : false,       # if you need synchronous requests, set this option to false
 *		url      : '',          # url to ajax processing file
 *		data     : '',          # data to be sent to the server
 *		callback : function(){} # callback function
 *	})
 */
ADM.sendData = function (params) {
    params = $.extend({
        url: '',
        data: '',
        callback: false
    }, params);
    $.ajax($.extend(params, {
        url: ADM.base_url() + params.url/*+"/"*/,
        data: params.data + '&rand=' + Math.random(),
        success: function (ans) {
            var answer = $.extend({
                data: false
            }, ans);
            if ((params.callback instanceof Function) && (answer.data !== false) && (answer.data != 'reload'))
                params.callback(answer.data);
            //remove and reinit LeviTi
            //$(".levitipouter").remove();
            //$('[title]').leviTip();
        }
    }));
}


/** Show dialog about confirm to delete
 *	Usage example:
 *	ADM.dialogDelete({
 *		title    : '',          # dialog title
 *		confirm  : '',          # confirm message
 *		url      : '',          # url to ajax processing file
 *		data     : '',          # data to be sent to the server
 *		callback : function(){} # callback function
 *	})
 */
ADM.dialogDelete = function (params) {
    var msg = $.extend({
        title: '',
        confirm: ''
    }, params);
    params = $.extend({
        url: '',
        data: '',
        callback: false
    }, params);

    if (confirm(i18['account_confirm_the_deleting']))
    {
        ADM.sendData({
            url: params.url,
            data: 'id=' + params.data,
            callback: params.callback
        });
    } else {
        return;
    }
    /* $("body").append("<div id='dialogDelete' class='dialog' title='" + msg.title + "'>\
     <span class='ui-icon ui-icon-alert'></span><p>" + msg.confirm + "</p></div>");
     
     var el = $("#dialogDelete");
     el.dialog({
     autoOpen: false,
     modal: true,
     show: 'fade',
     buttons: [// define buttons for form
     {
     text: i18['admin_cancel'],
     click: function () {
     $(this).dialog("close");
     }
     },
     {
     text: i18['admin_delete'],
     click: function () {
     ADM.sendData({
     url: params.url,
     data: 'id=' + params.data,
     callback: params.callback
     });
     $(this).dialog("close");
     }
     }
     ],
     close: function () {
     $(this).dialog("destroy").remove();
     }
     });
     el.dialog("open");*/
}


/** Show message after action
 *	msgtype string Type of message (error, notice, success)
 *	msgtext string Message body
 */
ADM.showMessage = function (msgtype, msgtext) {
    //	alert(msgtext);
    var icon;
    var mtype;
    switch (msgtype) {
        case 'error'   :
            icon = 'ui-icon-alert';
            mtype = 'danger';
            break;
        case 'notice'  :
            icon = 'ui-icon-notice';
            mtype = 'info';
            break;
        case 'success' :
            icon = 'ui-icon-check';
            mtype = 'success';
            break;
        default :
            icon = 'ui-icon-info';
            mtype = 'info';
    }
    if (typeof msgtext != 'undefined') {
        msg(msgtext, mtype);
    } else {
        //return;
    }
    //  alert("dd");
    /*$("body").append("<div id='dialogMsg' style='z-index:9999999999;' class='dialog' title=''><span class='ui-icon " + icon + "'></span><p>" + msgtext + "</p></div>");
     obj=$("#dialogMsg").dialog({
     buttons  : [  // define buttons for form
     {
     text : "OK", 
     click : function() {
     $(this).dialog("close");
     }
     }
     ],
     close: function(event, ui) {
     $(this).dialog("destroy").remove();
     }
     });*/
    //Close window after 3 sec
    //  setTimeout("obj.fadeOut(100,function(){$(this).remove()})",3000);

}

/* Default parameters for flexiGrid */
ADM.flex = {
    dataType: 'json',
    pagestat: i18['flex_pagestat'],
    pagetext: i18['flex_pagetext'],
    findtext: i18['flex_findtext'],
    errormsg: i18['flex_errormsg'],
    procmsg: i18['flex_procmsg'],
    nomsg: i18['flex_nomsg'],
    outof: i18['flex_outof']
};

/* Send keep alive ping */
ADM.ping = function () {
    ADM.sendData({
        url: '/admin/xhrping',
        data: 'ping_period=' + ADM.keepalive
    });
}

$(document).ready(function () {

    /* AJAX processing */
    $(document).ajaxStart(function () {
        if ($("#ajaxloader").length)
            return;
        $("body").append("<div id='ajaxloader'><div id='spinner'></div></div>");
    });
    $(document).ajaxStop(function () {
        $("#ajaxloader").hide(200, function () {
            $(this).remove()
        });
    });
    $(document).ajaxError(function (event, request, settings, exception) {
        //If error becous of timeout - no error windows!!(probably a bad internet and a lot of data processing or data server is overloaded) - try reload
        if (exception == 'timeout')// && $.cookie('ajax_driver_string'))
        {
            return false;
            //request cannot be sent time after time if it's about data saving,that's why check for url type before sent'
            //    var regV = /save/gi;     // search "save" at ajax url
            //   if(!$.cookie('ajax_driver_string').match(regV))// if "save" not fined - resend request
            //      setTimeout('ajax_driver("'+$.cookie('ajax_driver_string')+'")', 1000);
        } else if (exception == '')
            return false;
        else
            setTimeout("ADM.showMessage('error','" + i18['server_error'] + "')", 200);
    });
    $(document).ajaxSuccess(function (event, request, settings) {
        var answer = $.extend({
            data: false,
            msgtype: false,
            msgtext: false
        }, $.parseJSON(request.responseText));
        if (answer.msgtype && answer.msgtext) {
            setTimeout("ADM.showMessage('" + answer.msgtype + "','" + answer.msgtext + "')", 200);
        }
        if (answer.url)
        {
            var delay = $("form#login").length ? 1000 : 200;
            setTimeout("window.location.pathname ='" + answer.url + "'", delay);
        }
        if (answer.data == 'reload') {

            var delay = $("form#login").length ? 1000 : 200;
            setTimeout("window.location.pathname = '/admin'", delay);
        }

    });

    /* Update online status */
//ADM.ping();
    /* Keep alive ping */
//setInterval("ADM.ping()", ADM.keepalive * 1000);


});