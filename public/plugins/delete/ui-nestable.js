var defnest;
var UINestable = function () {
    //function to initiate jquery.nestable
    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
                output = list.data('output');

        if (window.JSON) {

            var send = window.JSON.stringify(list.nestable('serialize'));
            if (send != defnest)// make changes only if tree changed
            {
                if ($('#tree_type').val() == "menu") {
                    var method = "refresh_tree_menu/"+$("#root").val();
                } else {
                    var method = "refresh_tree";
                }
                console.log(window.JSON.stringify(list.nestable('serialize')));
                $.ajax({
                    type: "POST",
                    url: "/" + $("#lang").val() + "/admin/xhr_office/" + method,
                    data: send,
                    dataType: "json",
                    success: function (answer) {
                        if (answer.reload) {
                            window.location.reload();
                        }
                        if (answer.message) {
                            msg(answer.message);
                        }
                        defnest = send;//refresh def structure variable
                    }
                });
            }
            // output.val(window.JSON.stringify(list.nestable('serialize')));
            //, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };
    var runNestable = function () {
        // activate Nestable for list 1
        $('#nestable').nestable({
            group: 1,
            maxDepth: 20
        }).on('change', updateOutput);
        //store defoult structure for compare after moving the tree
        var e = $('#nestable').data('output', $('#nestable-output'));
        var list = e.length ? e : $(e.target),
                output = list.data('output');
        defnest = window.JSON.stringify(list.nestable('serialize'));
        //!

        // output initial serialised data
        //    updateOutput($('#nestable').data('output', $('#nestable-output')));
        $('#nestable-menu').on('click', function (e) {
            var target = $(e.target),
                    action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });
    };
    return {
        //main function to initiate template pages
        init: function () {
            runNestable();
        }
    };
}();