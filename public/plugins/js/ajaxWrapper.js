jQuery(document).ajaxSuccess(function(event, request, options) {
    if (options.dataType == 'json') {
        var data = JSON.parse(request.responseText);
        if(data.alert){
            $("#modal-info").find(".modal-body").html("<p>"+data.alert+"</p>");
            $("#modal-info").modal('show');
        }
    }
});