var adminEngine = function () {
    var runClicker = function (e) {//initialize click events
        $(document).on("click", "a.toggleWatch", function () {// watch toggle click using orm and id
            var that = $(this);
            var orm = that.data('orm');
            var id = that.data('id');
            $.ajax({
                type: "POST",
                url: '/admin/ajax/toggleWatch',
                dataType: 'json',
                data: {
                    orm: orm,
                    id: id
                },
                success: function (data) {
                    that.removeClass('btn-warning').removeClass('btn-success');
                    that.addClass(data.className);
                    that.text(data.text);
                    //console.log(data);
                }
            });
        });
    };
    var runAjaxWrapper = function (e) {//initialize ajax help functions
        $(document).ajaxSuccess(function (event, request, options) {
            if (options.dataType == 'json') {
                var data = JSON.parse(request.responseText);
                if (data.alert) {
                    msg(data.alert);
                } else if (data.reload) {
                    window.location.reload();
                }
            }
        });
    };
    var msg = function (message, type) {//run modal message
        $("#modal-info").find(".modal-body").html("<p>" + message + "</p>");
        $("#modal-info").modal('show');
    };
    return {
        //main function to initiate template pages
        init: function () {
            runAjaxWrapper();
            runClicker();
        }
    };
}();
$(document).ready(function () {//run adminEngine when document loaded
    adminEngine.init();
});