var Engine = function () {
    var runClicker = function (e) {//initialize click events
        /*$(document).on("click", "a.toggleWatch", function () {// watch toggle click using orm and id
            var that = $(this);
            var orm = that.data('orm');
            var id = that.data('id');
            $.ajax({
                type: "POST",
                url: '/admin/ajax/toggleWatch',
                dataType: 'json',
                data: {
                    orm: orm,
                    id: id
                },
                success: function (data) {
                    that.removeClass('btn-warning').removeClass('btn-success');
                    that.addClass(data.className);
                    that.text(data.text);
                    //console.log(data);
                }
            });
        });*/
    };
    var runSubmitter = function (e) {//initialize click events
        $(document).on("submit", "form.ajaxForm", function (e) {// run when any form class ajaxForm submited
            e.preventDefault();
            var that = $(this);
            that.find('input[type=submit],input[type=button]').hide();//hide to prevent multi clicks
            that.css('opacity','0.5');//loading process simulation
            var url = that.attr('action');
            var data = that.serialize();
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: data,
                success: function (data) {
                    console.log(data);
                    that.find('input[type=submit],input[type=button]').show();//show because action done
                    that.css('opacity','1');//dismiss loading effect because action done
                },
                error: function (data) {
                    if (data.status === 422) {//Validation fails
                        var errors = $.parseJSON(data.responseText);
                        $('.error .error-text').text('');
                        $('.error').removeClass('error');
                        $.each(errors.errors, function (key, value) {
                            $('[name=' + key + ']').closest('div').addClass('error');
                            $('[name=' + key + ']').closest('div').find('.error-text').text(value);
                        });
                        that.find('input[type=submit],input[type=button]').show();//show because action done
                        that.css('opacity','1');//dismiss loading effect because action done
                    }
                }
            });
        });
    };
    var runAjaxWrapper = function (e) {//initialize ajax help functions
        $(document).ajaxSuccess(function (event, request, options) {
            if (options.dataType == 'json') {
                var data = JSON.parse(request.responseText);
                if (data.alert) {
                    msg(data.alert);
                } else if (data.reload) {
                    window.location.reload();
                }
            }
        });
    };
    var msg = function (message, type) {//run modal message
        alert(message);
        // $("#modal-info").find(".modal-body").html("<p>" + message + "</p>");
        //$("#modal-info").modal('show');
    };
    return {
        //main function to initiate template pages
        init: function () {
            runAjaxWrapper();
            runSubmitter();
            // runClicker();
        }
    };
}().init();