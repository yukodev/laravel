@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')



    <h3 class="page-title">@lang('global.settings.setting_values_edit')</h3>
    @if(!$set->get('single') || count($setts)==0)
        <p>
            <a href="{{route('admin.settings.create',['setting_id'=>$set->get('id')])}}" class="btn btn-success">{{__('global.settings.create')}}</a>
        </p>
    @endif
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{$set->get('name')}}</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped {{ count($setts) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                <tr>
                    <th style="text-align:center;"><input type="checkbox" id="select-all"/></th>

                    <th>@lang('global.settings.fields.value')</th>
                    @if($set->get('use_short_text')==1)
                        <th>@lang('global.settings.fields.text')</th>
                    @endif
                    <th width="150">&nbsp;</th>

                </tr>
                </thead>

                <tbody>
                @if (count($setts) > 0)
                    @foreach ($setts as $o)
                        <tr data-entry-id="{{ $o->get('id') }}">
                            <td></td>

                            <td>{{ $o->get('value') }}</td>
                            @if($set->get('use_short_text')==1)
                                <td>{{ $o->get('short_text') }}</td>
                            @endif

                            <td width="150">
                                @if($o->watch==0)
                                    <a title='{{__("global.turn_on")}}' data-orm='SettingsValue' data-id='{{$o->id}}'
                                       class="btn btn-xs btn-warning toggleWatch">{{ __("global.not_act")}}</a>
                                @else
                                    <a title='{{__("global.turn_off")}}' data-orm='SettingsValue' data-id='{{$o->id}}'
                                       class="btn btn-xs btn-success toggleWatch">{{ __("global.act")}}</a>
                                @endif
                                <a href="{{ route('admin.settings.edit',[$o->get('id')]) }}"
                                   class="btn btn-xs btn-info">@lang('global.app_edit')</a>

                                {!! Form::open(array(
                                    'style' => 'display: inline-block;',
                                    'method' => 'DELETE',
                                    'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                    'route' => ['admin.settings.destroy', $o->get('id')])) !!}
                                {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                {!! Form::close() !!}
                            </td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

@endsection