@extends('layouts.app')

@section('content')
    <h3 class="page-title">{{$father->get('name')}}</h3>

    {!! Form::model($object, ['method' =>$method,'route' => $route, 'files' => true]) !!}

    <div class="box box-primary">
        <div class="box-header with-border">
            {{$title}}
        </div>

        <div class="box-body">
            <div class="row">
                {!! Form::hidden('setting_id',$father->id) !!}

                <div class="col-xs-12 form-group @if($errors->has('value')) has-error @endif">
                    {!! Form::label('value', __('global.settings.value'), ['class' => 'control-label']) !!}
                    {!! Form::text('value', old('value'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('value'))
                        <p class="help-block">
                            {{ $errors->first('value') }}
                        </p>
                    @endif
                </div>
                @if($father->use_class==1)
                    <div class="col-xs-12 form-group @if($errors->has('class')) has-error @endif">
                        {!! Form::label('class', __('global.settings.class'), ['class' => 'control-label']) !!}
                        {!! Form::text('class', old('class'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('class'))
                            <p class="help-block">
                                {{ $errors->first('class') }}
                            </p>
                        @endif
                    </div>
                @endif
                @if($father->use_img==1)
                    <div class="col-xs-12 form-group">
                        <div class="imageupload panel panel-default">
                            <div class="panel-heading clearfix">
                                <h3 class="panel-title pull-left">Upload Image</h3>
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn btn-default active">File</button>
                                    <button type="button" class="btn btn-default">URL</button>
                                </div>
                            </div>
                            <div class="file-tab panel-body">
                                @if($object->src)
                                    <img src="{{$object->src}}" data-def="{{ url('images') }}/no_photo.png"
                                         alt="Image preview" class="thumbnail object_img"
                                         style="max-width: 250px; max-height: 250px">
                                @else
                                    <img src="{{ url('images') }}/no_photo.png"
                                         data-def="{{ url('images') }}/no_photo.png" alt="Image preview"
                                         class="thumbnail object_img"
                                         style="max-width: 250px; max-height: 250px">
                                @endif
                                <input type="hidden" name="src" value="{{$object->src}}">

                            <!--<input class="thumbnail" style="max-width: 250px; max-height: 250px" src="{{$object->src}}" name="" type="image" value="">-->


                                <label class="btn btn-default btn-file">
                                    <span>Browse</span>
                                    <!-- The file is stored here. -->
                                    <input type="file" class="simple-upload" name="img-file">
                                </label>
                                <!--<button type="button" class="btn btn-default">Remove</button>-->
                            </div>
                            <div class="url-tab panel-body">
                                @if($object->src)
                                    <img src="{{$object->src}}" data-def="{{ url('images') }}/no_photo.png"
                                         alt="Image preview" class="thumbnail object_img"
                                         style="max-width: 250px; max-height: 250px">
                                @else
                                    <img src="{{ url('images') }}/no_photo.png"
                                         data-def="{{ url('images') }}/no_photo.png" alt="Image preview"
                                         class="thumbnail object_img"
                                         style="max-width: 250px; max-height: 250px">
                            @endif
                            <!--<input class="thumbnail" style="max-width: 250px; max-height: 250px" src="{{$object->src}}" name="" type="image" value="">-->

                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Image URL">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default">Submit</button>
                                    </div>
                                </div>
                                <!--<button type="button" class="btn btn-default">Remove</button>-->
                                <!-- The URL is stored here. -->
                                <input type="hidden" name="image-url">
                            </div>
                        </div>
                        <button type="button" id="imageupload-disable"
                                class="btn btn-danger">{{__('global.app.disable')}}</button>
                        <button type="button" id="imageupload-enable"
                                class="btn btn-success">{{__('global.app.enable')}}</button>
                        <button type="button" id="imageupload-reset"
                                class="btn btn-primary">{{__('global.app.reset')}}</button>
                    </div>
                @section('css')
                    <link rel="stylesheet"
                          href="{{ url('plugins/image-upload/dist/css') }}/bootstrap-imageupload.min.css"/>
                    @yield('css')
                @overwrite
                @section('javascript')
                    <script src="{{ url('plugins/image-upload/dist/js/bootstrap-imageupload.js') }}"></script>

                    <script>
                        var $imageupload = $('.imageupload');
                        $imageupload.imageupload({
                            allowedFormats: ['jpg', 'png', 'gif'],
                            maxFileSizeKb: 512
                        });
                        $('#imageupload-disable').on('click', function () {
                            $imageupload.imageupload('disable');
                            $(this).blur();
                        })

                        $('#imageupload-enable').on('click', function () {
                            $imageupload.imageupload('enable');
                            $(this).blur();
                        })
                        $('#imageupload-reset').on('click', function () {
                            $imageupload.imageupload('reset');
                            $(this).blur();
                        });
                    </script>
                    @yield('javascript')
                @overwrite
                @endif
                @if($father->use_phone==1)
                    <div class="col-xs-12 form-group @if($errors->has('phone')) has-error @endif">
                        {!! Form::label('phone', __('global.settings.phone'), ['class' => 'control-label']) !!}
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                            </div>
                            {!! Form::text('phone', old('phone'), ['class' => 'form-control', 'placeholder' => '(999) 999-9999']) !!}
                        </div>
                        <p class="help-block"></p>
                        @if($errors->has('phone'))
                            <p class="help-block">
                                {{ $errors->first('phone') }}
                            </p>
                        @endif
                    </div>
                @endif
                @if($father->use_email==1)
                    <div class="col-xs-12 form-group @if($errors->has('email')) has-error @endif">
                        {!! Form::label('email', __('global.settings.email'), ['class' => 'control-label']) !!}
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-envelope"></i>
                            </div>
                            {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => 'name@mail.com']) !!}
                        </div>
                        <p class="help-block"></p>
                        @if($errors->has('email'))
                            <p class="help-block">
                                {{ $errors->first('email') }}
                            </p>
                        @endif
                    </div>
                @endif
                @if($father->use_link_name==1)
                    <div class="col-xs-12 form-group">
                        {!! Form::label('link_name', __('global.settings.link_name'), ['class' => 'control-label']) !!}

                        {!! Form::text('link_name', old('link_name'), ['class' => 'form-control', 'placeholder' => '']) !!}

                    </div>
                @endif
                @if($father->use_link==1)
                    <div class="col-xs-12 form-group @if($errors->has('link')) has-error @endif">
                        {!! Form::label('link', __('global.settings.link'), ['class' => 'control-label']) !!}

                        <div class="input-group link">
                            <div class="input-group-addon">
                                <i class="fa fa-link"></i>
                            </div>

                            {!! Form::text('link', old('link'), ['class' => 'form-control', 'placeholder' => 'http://domain.com']) !!}
                        </div>
                        <p class="help-block"></p>
                        @if($errors->has('link'))
                            <p class="help-block">
                                {{ $errors->first('link') }}
                            </p>
                        @endif
                    </div>
                    <div class="col-xs-12 form-group">
                        {!! Form::label('link_target', __('global.settings.link_target'), ['class' => 'control-label']) !!}

                        {!! Form::select('link_target', ['_self'=>'_self','_blank'=>'_blank'], old('link_target'), ['class' => 'form-control select22', 'required' => '']) !!}

                    </div>
                @endif
                @if($father->use_date==1)
                    <div class="col-xs-12 form-group @if($errors->has('date')) has-error @endif">
                        {!! Form::label('date', __('global.settings.date'), ['class' => 'control-label']) !!}
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('date', old('date'), ['class' => 'form-control datepicker', 'placeholder' => '', 'required' => '']) !!}
                        </div>
                        <p class="help-block"></p>
                        @if($errors->has('date'))
                            <p class="help-block">
                                {{ $errors->first('date') }}
                            </p>
                        @endif
                    </div>
                @section('javascript')
                    <script src="{{ url('adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
                    <script>
                        $('.datepicker').datepicker({
                            autoclose: true,
                            format: 'yyyy-mm-dd'
                        })
                    </script>
                    @yield('javascript')
                @overwrite
                @section('css')
                    <link rel="stylesheet"
                          href="{{ url('adminlte/plugins/datepicker') }}/datepicker3.css"/>
                    @yield('css')
                @overwrite
                @endif


                @if($father->use_short_text==1)
                    <div class="col-xs-12 form-group">
                        {!! Form::label('short_text', __('global.settings.short_text'), ['class' => 'control-label']) !!}
                        {!! Form::textarea('short_text', old('short_text'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}

                    </div>
                @endif
                @if($father->use_text==1)

                    <div class="col-xs-12 form-group">
                        {!! Form::label('text', __('global.settings.text'), ['class' => 'control-label']) !!}
                        @if($father->use_editor==1)
                            {!! Form::textarea('text', old('text'), ['class' => 'form-control ckeditor', 'placeholder' => '', 'required' => '']) !!}
                        @section('javascript')
                            <script src="{{ url('plugins/ckeditor/ckeditor.js') }}"></script>
                            <script src="{{ url('plugins/ckfinder/ckfinder.js') }}"></script>
                            <script>
                                CKFinder.setupCKEditor(CKEDITOR, '/plugins/ckfinder/');
                            </script>
                            @yield('javascript')
                        @overwrite
                        @else
                            {!! Form::textarea('text', old('text'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                        @endif

                    </div>
                @endif
            </div>


        </div>
    </div>

    {!! Form::submit(trans('global.app_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

