@extends('layouts.app')

@section('content')
    <h3 class="page-title">{{$title}}</h3>

    {!! Form::model($object, ['method' => $method,'route' => $route, 'files' => true]) !!}

    <div class="box box-primary">
        {{--<div class="box-header with-border">
            @lang('global.app_edit')
        </div>--}}

        <div class="box-body">
            <div class="row">
                <div class="col-xs-12 form-group @if($errors->has('name')) has-error @endif">
                    {!! Form::label('name', __('global.catalog.name'), ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>

                <div class="col-xs-12 form-group @if($errors->has('name_trans')) has-error @endif">
                    {!! Form::label('name_trans', __('global.catalog.name_trans'), ['class' => 'control-label']) !!}
                    {!! Form::text('name_trans', old('name_trans'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name_trans'))
                        <p class="help-block">
                            {{ $errors->first('name_trans') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-12 form-group">
                    {!! Form::label('watch', __('global.catalog.watch'), ['class' => 'control-label']) !!}

                    {!! Form::select('watch', ['0'=>__('global.app_watch0'),'1'=>'global.app_watch1'], old('watch'), ['class' => 'form-control select22', 'required' => '']) !!}

                </div>

                <div class="col-xs-12 form-group">
                    {!! Form::label('short_text', __('global.catalog.short_text'), ['class' => 'control-label']) !!}
                    {!! Form::textarea('short_text', old('short_text'), ['class' => 'form-control', 'placeholder' => '', 'required' => false]) !!}

                </div>

                <div class="col-xs-12 form-group">
                    {!! Form::label('text', __('global.catalog.text'), ['class' => 'control-label']) !!}
                    {!! Form::textarea('text', old('text'), ['class' => 'form-control ckeditor', 'placeholder' => '', 'required' => '']) !!}
                    @section('javascript')
                        <script src="{{ url('plugins/ckeditor/ckeditor.js') }}"></script>
                        <script src="{{ url('plugins/ckfinder/ckfinder.js') }}"></script>
                        <script>
                            CKFinder.setupCKEditor(CKEDITOR, '/plugins/ckfinder/');
                        </script>
                        @yield('javascript')
                    @overwrite


                </div>
            </div>


        </div>
    </div>

    {!! Form::submit(trans('global.app_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

