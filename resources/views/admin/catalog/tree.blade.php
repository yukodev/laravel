@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('global.catalog.tree')</h3>
    <p>
        <a href="{{route('admin.catalog.create')}}" class="btn btn-success">{{__('global.catalog.create')}}</a>
    </p>

    <div class="box box-primary">
        <div class="box-header with-border">
            @lang('global.catalog.tree')
        </div>

        <div class="box-body">


                <div class="dd" id="nestable">
                    <ol class='dd-list dd3-list'>
                        {!!$nestable!!}
                    </ol>
                </div>


        </div>
    </div>
@section('javascript')

    <script src="{{ url('plugins/nestable2/jquery.nestable.min.js') }}"></script>
    <script src="{{ url('plugins/nestable2/ui-nestable.js') }}"></script>
    <script>
        var nestableUrl="{{route('admin.refreshCategoriesTree')}}";
        UINestable.init(
            {
                maxDepth: 1
            }
        );
    </script>
    @yield('javascript')
@overwrite
@section('css')
    <link rel="stylesheet"
          href="{{ url('plugins/nestable2/jquery.nestable.min.css') }}"/>
    <link rel="stylesheet"
          href="{{ url('plugins/nestable2/nestableCustom.css') }}"/>

    @yield('css')
@overwrite
@stop

