@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')




    <div class="box">
        <div class="box-header">
            <h3 class="box-title">@lang('global.texts.texts_list')</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered no-paginate table-striped1 {{ count($objects) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                <tr>
                    <th style="text-align:center;"><input type="checkbox" id="select-all"/></th>

                    <th>@lang('global.texts.fields.name')</th>
                    <th>@lang('global.settings.fields.short_text')</th>
                    <th>&nbsp;</th>

                </tr>
                </thead>

                <tbody>
                @if (count($objects) > 0)
                    @foreach ($objects as $o)
                        <tr data-entry-id="{{ $o->get('id') }}">
                            <td></td>

                            <td>{{ $o->get('name') }}</td>
                            <td>{{ $o->get('short_text') }}</td>

                            <td>
                                @if($o->watch==0)
                                    <a title='{{__("global.turn_on")}}' data-orm='Text' data-id='{{$o->id}}'
                                       class="btn btn-xs btn-warning toggleWatch">{{ __("global.not_act")}}</a>
                                @else
                                    <a title='{{__("global.turn_off")}}' data-orm='Text' data-id='{{$o->id}}'
                                       class="btn btn-xs btn-success toggleWatch">{{ __("global.act")}}</a>
                                @endif
                                <a href="{{ route('admin.texts.edit',[$o->get('id')]) }}"
                                   class="btn btn-xs btn-info">@lang('global.app_edit')</a>

                                {!! Form::open(array(
                                    'style' => 'display: inline-block;',
                                    'method' => 'DELETE',
                                    'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                    'route' => ['admin.texts.destroy', $o->get('id')])) !!}
                                {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                {!! Form::close() !!}
                            </td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                    </tr>
                @endif
                </tbody>
            </table>
            <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                {{$objects->links()}}
            </div>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

@endsection