@extends('layouts.app')

@section('content')
    <h3 class="page-title">{{$title}}</h3>

    {!! Form::model($object, ['method' => $method,'route' => $route, 'files' => true]) !!}
    @include('partials.msgs')

    <div class="box box-primary">
        {{--<div class="box-header with-border">
            @lang('global.app_edit')
        </div>--}}

        <div class="box-body">
            <div class="row">
                <div class="col-xs-12 form-group @if($errors->has('name')) has-error @endif">
                    {!! Form::label('name', __('global.menus.name'), ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>

                <div class="col-xs-12 form-group @if($errors->has('name_trans')) has-error @endif">
                    {!! Form::label('name_trans', __('global.menus.name_trans'), ['class' => 'control-label']) !!}
                    {!! Form::text('name_trans', old('name_trans'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name_trans'))
                        <p class="help-block">
                            {{ $errors->first('name_trans') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-12 form-group">
                    {!! Form::label('texts_type_id', __('global.menus.fields.texts_type_id'), ['class' => 'control-label']) !!}

                    {!! Form::select('texts_type_id', $texts_types, old('texts_type_id'), ['class' => 'form-control select22', 'required' => '']) !!}

                </div>
                <div class="col-xs-12 form-group">
                    <div class="imageupload panel panel-default">
                        <div class="panel-heading clearfix">
                            <h3 class="panel-title pull-left">Upload Image</h3>
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-default active">File</button>
                                <button type="button" class="btn btn-default">URL</button>
                            </div>
                        </div>
                        <div class="file-tab panel-body">
                            @if($object->src)
                                <img src="{{$object->src}}" data-def="{{ url('images') }}/no_photo.png" alt="Image preview" class="thumbnail object_img"
                                     style="max-width: 250px; max-height: 250px">
                            @else
                                <img src="{{ url('images') }}/no_photo.png" data-def="{{ url('images') }}/no_photo.png" alt="Image preview" class="thumbnail object_img"
                                     style="max-width: 250px; max-height: 250px">
                            @endif

                            <input type="hidden" name="src" value="{{$object->src}}">
                        <!--<input class="thumbnail" style="max-width: 250px; max-height: 250px" src="{{$object->src}}" name="" type="image" value="">-->

                            <label class="btn btn-default btn-file">
                                <span>Browse</span>
                                <!-- The file is stored here. -->
                                <input type="file" class="simple-upload" name="img-file">

                            </label>
                            <!--<button type="button" class="btn btn-default">Remove</button>-->
                        </div>
                        <div class="url-tab panel-body">
                            @if($object->src)
                                <img src="{{$object->src}}" data-def="{{ url('images') }}/no_photo.png" alt="Image preview" class="thumbnail object_img"
                                     style="max-width: 250px; max-height: 250px">
                            @else
                                <img src="{{ url('images') }}/no_photo.png" data-def="{{ url('images') }}/no_photo.png" alt="Image preview" class="thumbnail object_img"
                                     style="max-width: 250px; max-height: 250px">
                            @endif

                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Image URL">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default">Submit</button>
                                </div>
                            </div>
                            <!--<button type="button" class="btn btn-default">Remove</button>-->
                            <!-- The URL is stored here. -->
                            <input type="hidden" name="image-url">
                        </div>
                    </div>
                    <button type="button" id="imageupload-disable" class="btn btn-danger">{{__('global.app.disable')}}</button>
                    <button type="button" id="imageupload-enable" class="btn btn-success">{{__('global.app.enable')}}</button>
                    <button type="button" id="imageupload-reset" class="btn btn-primary">{{__('global.app.reset')}}</button>

                </div>
                @section('css')
                    <link rel="stylesheet"
                          href="{{ url('plugins/image-upload/dist/css') }}/bootstrap-imageupload.min.css"/>
                    @yield('css')
                @overwrite
                @section('javascript')
                    <script src="{{ url('plugins/image-upload/dist/js/bootstrap-imageupload.js') }}"></script>

                    <script>
                        var $imageupload = $('.imageupload');
                        $imageupload.imageupload({
                            allowedFormats: ['jpg','png','gif'],
                            maxFileSizeKb: 512
                        });
                        $('#imageupload-disable').on('click', function() {
                            $imageupload.imageupload('disable');
                            $(this).blur();
                        })

                        $('#imageupload-enable').on('click', function() {
                            $imageupload.imageupload('enable');
                            $(this).blur();
                        })
                        $('#imageupload-reset').on('click', function () {
                            $imageupload.imageupload('reset');
                            $(this).blur();
                        });
                    </script>
                    @yield('javascript')
                @overwrite

                <div class="col-xs-12 form-group">
                    {!! Form::label('watch', __('global.menus.watch'), ['class' => 'control-label']) !!}

                    {!! Form::select('watch', ['0'=>__('global.app_watch0'),'1'=>'global.app_watch1'], old('watch'), ['class' => 'form-control select22', 'required' => '']) !!}

                </div>

                <div class="col-xs-12 form-group">
                    {!! Form::label('short_text', __('global.menus.short_text'), ['class' => 'control-label']) !!}
                    {!! Form::textarea('short_text', old('short_text'), ['class' => 'form-control', 'placeholder' => '', 'required' => false]) !!}

                </div>

                <div class="col-xs-12 form-group">
                    {!! Form::label('text', __('global.menus.text'), ['class' => 'control-label']) !!}
                    {!! Form::textarea('text', old('text'), ['class' => 'form-control ckeditor', 'placeholder' => '', 'required' => '']) !!}
                    @section('javascript')
                        <script src="{{ url('plugins/ckeditor/ckeditor.js') }}"></script>
                        <script src="{{ url('plugins/ckfinder/ckfinder.js') }}"></script>
                        <script>
                            CKFinder.setupCKEditor(CKEDITOR, '/plugins/ckfinder/');
                        </script>
                        @yield('javascript')
                    @overwrite


                </div>
            </div>


        </div>
    </div>

    {!! Form::submit(trans('global.app_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

