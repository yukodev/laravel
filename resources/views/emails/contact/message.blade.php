@component('mail::message')
# {{__('global.mail.hello')}}
<p>
{{__('global.mail.new_site_mail_message')}}
</p>
<p>
{{__('global.mail.site_messages_details')}}
</p>
@foreach($data as $key=>$o)
@if($o!='' && $key!='_token')
<p>
<bold>{{__("global.fields.$key")}}:</bold> {{$o}}
</p>
@endif
@endforeach
@component('mail::button', ['url' => route('WelcomeController')])
{{__('global.mail.go_to_site')}}
@endcomponent
{{__('global.mail.thanks')}},<br>
{{ $site_name }}
@endcomponent
