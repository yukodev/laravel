@extends('layouts.appFront')

@section('content')
    <div class="contact-columns">
        <div class="col">
            <span class="title-contact">Контакты</span>
            <div class="contact-box">
                <span class="address row-contact">{{$site_name->get('short_text')}}</span>
                <span class="phone row-contact">{{$site_name->get('phone')}}</span>
                <span class="email row-contact"><a
                            href="mailto:{{$site_name->get('email')}}">{{$site_name->get('email')}}</a></span>
                @if(count($social)>0)
                    <ul class="social-list">
                        @foreach($social as $o)
                            <li>
                                <a href="{{$o->get('link')}}" title="{{$o->get('value')}}"
                                   target="{{$o->get('link_target')}}" class="{{$o->get('class')}}"></a>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
        <div class="col">
            <span class="title-contact">Написать нам</span>
            {!! Form::model($object, ['method' => 'POST','route' => ['mailer'], 'class' => 'form-contact form ajaxForm']) !!}

            <fieldset>
                <div class="row-form">
                    <div class="col-form">
                        <div class="row">
                            {!! Form::text('name', '', ['placeholder' => __('front.forms.labels.your_name'), 'required' => '']) !!}
                            <span class="error-text"></span>
                        </div>
                        <div class="row">
                            {!! Form::text('phone', '', ['placeholder' => __('front.forms.labels.your_phone'), 'required' => '']) !!}
                            <span class="error-text"></span>
                        </div>
                        <div class="row">
                            {!! Form::text('email', '', ['placeholder' => __('front.forms.labels.your_email'), 'required' => '']) !!}
                            <span class="error-text"></span>
                        </div>
                    </div>
                    <div class="col-form">
                        <div class="row">
                            {!! Form::textarea('short_text', '', ['class' => 'txt_text','cols'=>'30', 'placeholder' =>  __('front.forms.labels.message'), 'required' => false]) !!}
                        </div>
                    </div>
                </div>
                <div class="row-form">
                    <div class="col-form">
                        <img src="{{ url('frontend/images/') }}/ico.png" alt="image description">
                    </div>
                    <div class="col-form">
                        <div class="row">
                            {!! Form::submit(__('front.send')) !!}
                        </div>
                    </div>
                </div>
            </fieldset>
            {!! Form::close() !!}
        </div>
    </div>
    @if($contacts_map->text)
    <div id="map">
        {!! $contacts_map->get('text') !!}
    </div>
    @endif
@endsection
