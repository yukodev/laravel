@extends('layouts.appFront')

@section('content')
    <div class="content-home">
        <span class="title-content">{{$welcome_slogan->get('value')}}</span>
        <p class="small-box">{{$welcome_slogan->get('short_text')}}</p>
        <div class="properties-columns">
            @if(count($advantages_left)>0)
                <div class="col wow slideInLeft" data-wow-duration="2.5s" data-wow-delay="0s"
                     data-wow-offset="0">
                    @foreach($advantages_left as $o)
                        <div class="row">
                            <span class="title {{$o->get('class')}}">{{$o->get('value')}}</span>
                            <p>{{$o->get('short_text')}}</p>
                        </div>
                    @endforeach
                </div>
            @endif
            @if(count($advantages_right)>0)
                <div class="col wow slideInRight" data-wow-duration="2.5s" data-wow-delay="0s"
                     data-wow-offset="0">
                    @foreach($advantages_right as $o)
                        <div class="row">
                            <span class="title {{$o->get('class')}}">{{$o->get('value')}}</span>
                            <p>{{$o->get('short_text')}}</p>
                        </div>
                    @endforeach
                </div>
            @endif
            <div class="text-col">
                <span>Если в холодильнике <br/>нет колбасы, <br/>то мужчина считает, <br/>что в доме нет еды…</span>
                <img class="wow fadeInDown" data-wow-duration="2.5s" data-wow-delay="0s" data-wow-offset="0"
                     src="{{ url('frontend/images/') }}/img10.png" alt="image description"/>
            </div>
        </div>
    </div>
    <div class="content-home">
        {{-- <span class="title-content">Немного о нас</span>--}}
        {!! $object->get('text') !!}

    </div>
    <div class="content-home new-promo">
        <img src="{{ url('frontend/images/') }}/img11.png" class="picture-ico wow bounceInDown"
             data-wow-duration="2.5s"
             data-wow-delay="0s" data-wow-offset="0" alt="image description"/>
        <span class="title-content">{{$promo_title->get('value')}}</span>
        <p>{{$promo_title->get('short_text')}}</p>
        @if(count($promo_items)>0)
            <ul class="list-promo">
                @foreach($promo_items as $o)
                <li>
                    <a target="{{$o->get('target')}}" href="{{$o->get('link')}}" class="wow zoomIn" data-wow-duration="2.5s" data-wow-delay="0s"
                       data-wow-offset="0">
                        <img src="{{$o->get('src')}}" alt="{{$o->get('value')}}"/>
                        <div class="text">
                            <strong>{{$o->get('value')}}</strong>
                            <span>{{$o->get('short_text')}}</span>
                        </div>
                    </a>
                </li>
                @endforeach
            </ul>
        @endif
    </div>
    <div class="content-home">
        <span class="title-content">Отзывы потребителей</span>
        <p class="small-box">Более 3000 украинских и зарубежных потребителей нашей продукции уже на
            протяжении многих лет выбирают нашу продукцию!</p>
        <section class="comment-conrainer">
            <article class="post-comment">
                <div class="picture"><img src="{{ url('frontend/images/') }}/img5.png"
                                          alt="image description"></div>
                <div class="text">
                    <span class="title">Качественные и очень вкусные продукты!</span>
                    <p>Широкий ассортимент - от сосисок и колбас для ежедневного употребления до изысканных
                        мясных деликатесов.</p>
                    <em class="date"><span>Александр, Львов</span> (29.05.2017)</em>
                </div>
            </article>
            <article class="post-comment">
                <div class="picture"><img src="{{ url('frontend/images/') }}/img5.png"
                                          alt="image description"></div>
                <div class="text">
                    <span class="title">Качественные и очень вкусные продукты!</span>
                    <p>Широкий ассортимент - от сосисок и колбас для ежедневного употребления до изысканных
                        мясных деликатесов.</p>
                    <em class="date"><span>Александр, Львов</span> (29.05.2017)</em>
                </div>
            </article>
            <article class="post-comment">
                <div class="picture"><img src="{{ url('frontend/images/') }}/img5.png"
                                          alt="image description"></div>
                <div class="text">
                    <span class="title">Качественные и очень вкусные продукты!</span>
                    <p>Широкий ассортимент - от сосисок и колбас для ежедневного употребления до изысканных
                        мясных деликатесов.</p>
                    <em class="date"><span>Александр, Львов</span> (29.05.2017)</em>
                </div>
            </article>
        </section>
    </div>
@endsection
