@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">

            <li class="{{ $request->segment(1) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/') }}">
                    <i class="fa fa-wrench"></i>
                    <span class="title">@lang('global.app_dashboard')</span>
                </a>
            </li>

            @can('users_manage')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span class="title">@lang('global.user-management.title')</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">

                        <li class="{{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }}">
                            <a href="{{ route('admin.permissions.index') }}">
                                <i class="fa fa-briefcase"></i>
                                <span class="title">
                                @lang('global.permissions.title')
                            </span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                            <a href="{{ route('admin.roles.index') }}">
                                <i class="fa fa-briefcase"></i>
                                <span class="title">
                                @lang('global.roles.title')
                            </span>
                            </a>
                        </li>
                        <li class="{{ $request->segment(2) == 'users' ? 'active active-sub' : '' }}">
                            <a href="{{ route('admin.users.index') }}">
                                <i class="fa fa-user"></i>
                                <span class="title">
                                @lang('global.users.title')
                            </span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endcan
            <li class="treeview{{ ($request->segment(2) == 'settings') ? ' menu-open':''}}">
                <a href="#">
                    <i class="fa fa-cog"></i>
                    <span class="title">@lang('global.settings.settings')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" @if($request->segment(2) == 'settings') style='display:block'@endif>
                    @foreach($settings as $o)
                        <li class="{{ (Request::url() == route('admin.settings.show',['id'=>$o->id])) ? 'active active-sub' : '' }}">
                            <a href="{{ route('admin.settings.show',['id'=>$o->id]) }}">
                                <i class="fa fa-circle-o"></i>
                                <span class="title">
                                {!! $o->get('name') !!}
                            </span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </li>
            <li class="treeview{{ ($request->segment(2) == 'catalog') ? ' menu-open':''}}">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span class="title">@lang('global.catalog.menus')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" @if($request->segment(2) == 'catalog') style='display:block'@endif>
                    <li class="{{ (Request::url() == route('admin.catalog.index')) ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.catalog.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span class="title">
                                @lang('global.catalog.tree')
                            </span>
                        </a>
                    </li>
                    <li class="{{ (Request::url() == route('admin.catalog.create')) ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.catalog.create') }}">
                            <i class="fa fa-circle-o"></i>
                            <span class="title">
                                @lang('global.catalog.create')
                            </span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview{{ ($request->segment(2) == 'products') ? ' menu-open':''}}">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span class="title">@lang('global.products.products')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" @if($request->segment(2) == 'products') style='display:block'@endif>
                    <li class="{{ (Request::url() == route('admin.products.index')) ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.products.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span class="title">
                                @lang('global.products.products_list')
                            </span>
                        </a>
                    </li>
                    <li class="{{ (Request::url() == route('admin.products.create')) ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.products.create') }}">
                            <i class="fa fa-circle-o"></i>
                            <span class="title">
                                @lang('global.products.create')
                            </span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview{{ ($request->segment(2) == 'menus') ? ' menu-open':''}}">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span class="title">@lang('global.menus.menus')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" @if($request->segment(2) == 'menus') style='display:block'@endif>
                    <li class="{{ (Request::url() == route('admin.menus.index')) ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.menus.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span class="title">
                                @lang('global.menus.tree')
                            </span>
                        </a>
                    </li>
                    <li class="{{ (Request::url() == route('admin.menus.create')) ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.menus.create') }}">
                            <i class="fa fa-circle-o"></i>
                            <span class="title">
                                @lang('global.menus.create')
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview{{ ($request->segment(2) == 'texts') ? ' menu-open':''}}">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span class="title">@lang('global.texts.texts')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" @if($request->segment(2) == 'texts') style='display:block'@endif>
                    <li class="{{ (Request::url() == route('admin.texts.index')) ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.texts.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span class="title">
                                @lang('global.texts.texts_list')
                            </span>
                        </a>
                    </li>
                    <li class="{{ (Request::url() == route('admin.texts.create')) ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin.texts.create') }}">
                            <i class="fa fa-circle-o"></i>
                            <span class="title">
                                @lang('global.texts.create')
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="{{ $request->segment(1) == 'change_password' ? 'active' : '' }}">
                <a href="{{ route('auth.change_password') }}">
                    <i class="fa fa-key"></i>
                    <span class="title">{{__('global.users.change_password')}}</span>
                </a>
            </li>

            <li>
                <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('global.app_logout')</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">@lang('global.logout')</button>
{!! Form::close() !!}
