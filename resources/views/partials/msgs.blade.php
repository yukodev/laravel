@if (\Session::has('msgs'))
    <div class="alert alert-success">
        <ul>
            @foreach(Session::get('msgs') as $one)
                <li> {!! $one !!}</li>
            @endforeach

        </ul>
    </div>
@endif