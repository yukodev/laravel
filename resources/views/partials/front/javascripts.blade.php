<script src="{{ url('frontend/js/') }}/jquery-1.12.4.js"></script>
<script src="{{ url('frontend/js/') }}/jquery-ui.js"></script>
<script src="{{ url('frontend/js/') }}/jquery.simpler-sidebar.min.js"></script>
<script src="{{ url('frontend/js/') }}/visuallightbox.js"></script>
<script src="{{ url('frontend/js/') }}/slick.min.js"></script>
<script src="{{ url('frontend/js/') }}/wow.min.js"></script>
<script src="{{ url('frontend/js/') }}/visuallightbox.js"></script>
<script src="{{ url('frontend/js/') }}/jquery.main.js"></script>
<script src="{{ url('frontend/js/') }}/jquery.inview.min.js"></script>
<!--[if IE]>
<script src="{{ url('frontend/js/') }}/ie.js"></script>
<![endif]-->
<script src="{{ url('frontend/plugins/') }}/engine.js"></script>
<script>
    window._token = '{{ csrf_token() }}';
    $.ajaxSetup({
        beforeSend: function(xhr, type) {
            if (!type.crossDomain) {
                xhr.setRequestHeader('X-CSRF-Token', window._token);
            }
        },
    });
</script>



@yield('javascript')