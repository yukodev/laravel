<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0 "/>
<title>Карпат-Продукт</title>
<link media="all" rel="stylesheet" href="{{ url('frontend/css/') }}/lightbox.css">
<link media="all" rel="stylesheet" href="{{ url('frontend/css/') }}/slick.css">
<link media="all" rel="stylesheet" href="{{ url('frontend/css/') }}/fancybox.css">
<link media="all" rel="stylesheet" href="{{ url('frontend/css/') }}/animate.min.css">
<link media="all" rel="stylesheet" href="{{ url('frontend/css/') }}/all.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,500,400,400i,300i,700,900&amp;subset=latin-ext"
      rel="stylesheet">
<!--[if lt IE 9]>
<link rel="stylesheet" href="{{ url('frontend/css/') }}/ie.css"/>
<![endif]-->
@yield('css')