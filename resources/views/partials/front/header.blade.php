<header id="header">
    <div id="nav">
        <a href="#"
           class="opener"><span></span><span></span><span></span><span></span><span></span><span></span></a>
        <nav>
            <ul>{!! $menus !!}</ul>
        </nav>
    </div>
    <div class="function-block">
        <ul class="account-box">
            <li class="account">
                <span><a href="#popup2" class="lightbox">Вход</a></span>
                <span><a href="#popup1" class="lightbox">Регистрация</a></span>
            </li>
            <li class="basket">
                <span><a href="#">Моя корзина</a></span>
                <span>Товаров: <strong>15</strong></span>
            </li>
        </ul>
    </div>
</header>