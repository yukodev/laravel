<footer id="footer">
    @if($use_footer)
        <div class="footer-holder">
            <div class="footer-form">
                {!! Form::model($object, ['method' => 'POST','route' => ['mailer'], 'class' => 'feedback-form ajaxForm']) !!}
                <fieldset>
                    <p>{{__('front.footer.form_title')}}</p>
                    <div class="text">
                        {!! Form::text('name', '', ['class' => 'txt_name', 'placeholder' => '', 'required' => '']) !!}
                        <span class="error-text"></span>
                    </div>
                    <div class="text">
                        {!! Form::text('phone', '', ['class' => 'txt_phone', 'placeholder' => '', 'required' => '']) !!}
                        <span class="error-text"></span>
                    </div>
                    <div class="text">
                        {!! Form::text('email', '', ['class' => 'txt_email', 'placeholder' => '', 'required' => '']) !!}
                        <span class="error-text"></span>
                    </div>
                    <div class="text">
                        {!! Form::textarea('short_text', '', ['class' => 'txt_text','cols'=>'30', 'placeholder' => '', 'required' => false]) !!}
                        <span class="error-text"></span>
                    </div>
                    {!! Form::submit(__('front.send')) !!}

                </fieldset>
                {!! Form::close() !!}
            </div>
            <div class="footer-col">
                <div class="row">
                    <strong class="add-logo wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.5s"
                            data-wow-offset="0"><a href="{{route('WelcomeController')}}"></a></strong>
                    <address>
                        <p>
                            <span>{{$site_name->get("short_text")}}</span>
                            <span><a href="mailto:{{$site_name->get("email")}}">{{$site_name->get("email")}}</a></span>
                        </p>
                        @if(count($phones)>0)
                            @foreach($phones as $o)
                                <span class="phone">{{$o->get('value')}}<span>{{$o->get('short_text')}}</span></span>
                            @endforeach
                        @endif
                    </address>
                </div>
                <span class="title">{{$site_name->get("value")}}</span>
                <p>{{$site_name->get("text")}}</p>
                <span class="copyright">{{date("Y")}} &copy; Продукты Карпат. Все права защищены. <br/>Сайт изготовлен: <a
                            href="#">Web-for-business</a></span>
            </div>
        </div>
    @endif
    <nav class="footer-nav">
        <ul>
            <li><a href="#">Кнопка меню</a></li>
            <li><a href="#">Кнопка меню</a></li>
            <li><a href="#">Кнопка меню</a></li>
            <li><a href="#">Кнопка меню</a></li>
            <li><a href="#">Кнопка меню</a></li>
            <li><a href="#">Кнопка меню</a></li>
        </ul>
    </nav>
</footer>