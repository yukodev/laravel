<!DOCTYPE html>
<html lang="ua">
<head>
    @include('partials.front.head')
</head>
<body class="home">
<div id="wrapper">
    <div class="wrapper-holder">
        @include('partials.front.header')

        <div class="intro">
            <img src="{{$header_data->get('src')}}" alt="{{$header_data->get('value')}}">
            <div class="logo-box wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.5s" data-wow-offset="0">
                <strong class="logo"><a href="{{route('WelcomeController')}}">{{$site_name->get('value')}}</a></strong>
                {!! $header_data->get('text') !!}

            </div>
        </div>
        <div id="main">
            <div id="content">
                @yield('content')

            </div>
        </div>

        @include('partials.front.footer')


    </div>
</div>
<div class="popup-holder">
    <div id="popup1" class="lightbox">
        <span class="title">Регистрация</span>
        <form action="#" class="form">
            <fieldset>
                <div class="row"><input type="text" placeholder="Ваше имя"/></div>
                <div class="row"><input type="text" placeholder="Ваш телефон"/></div>
                <div class="row"><input type="text" placeholder="Ваш e-mail"/></div>
                <div class="row"><input type="password" placeholder="Ваш пароль"/></div>
                <div class="row"><input type="button" value="Зарегистрироваться"/></div>
            </fieldset>
        </form>
    </div>
    <div id="popup2" class="lightbox">
        <span class="title">Вход</span>
        <form action="#" class="form">
            <fieldset>
                <div class="row"><input type="text" placeholder="Ваше имя"/></div>
                <div class="row"><input type="password" placeholder="Ваш пароль"/></div>
                <div class="row"><input class="small" type="button" value="Войти"/></div>
            </fieldset>
        </form>
    </div>
</div>
@include('partials.front.javascripts')
</body>
</html>