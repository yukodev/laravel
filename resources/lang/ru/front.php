<?php

return [
    'footer' => [
        'form_title' => 'Вы можете связаться с нами по любому интересующему вас вопросу'
    ],
    'fields' => [
        'name' => 'Имя'
    ],
    'send' => 'Отправить',
    'forms' => [
        'labels' => [
            'your_name' => 'Ваше имя',
            'your_phone' => 'Ваше телефон',
            'your_email' => 'Ваше e-mail',
            'your_message' => 'Ваше имя',
            'message' => 'Текст сообщения'
        ]
    ]
];